﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ModelsTablesDBLib;
using Bearing;

namespace IRI_Distribution
{
    // Structs *************************************************************************************

    // ---------------------------------------------------------------------------------------------
    // Вложенная структура СП 

    public struct SP_IN
    {
        // диапазон частот в КГц сектора
        public double Fmin;
        public double Fmax;

        // границы сектора по азимуту в град 0...360 по часовой стрелке
        // от СП
        public double Beta_min;
        public double Beta_max;

        // Номер литеры диапазона (0...)
        // !!! Для уже перераспределенных диапазонов
        public int NumLit;

    }
    // ---------------------------------------------------------------------------------------------
    // Структура СП (станции помех)

    public struct S_SP_DistrIRI
    {
        // Номер СП
        public int N_SP;

        // X(км) координата X на плоскости
        public double X_SP;
        // Y(км) координата Y на плоскости (север)
        public double Y_SP;
        // Z(км) координата Z высота
        public double Z_SP;

        // Макс. кол-во ИРИ на литеру
        public int MaxIRILit;

        // Массив работы по литерам (1-работает по этой литере)
        public int[] Lit;

        // Массив вложенных структур по максимальному числу секторов
        // (диапазоны частот и углов)
        public SP_IN[] mas_SP_IN;

    }
    // ---------------------------------------------------------------------------------------------
    // Распределение ИРИ

    public struct S_IRI_DistrIRI
    {
        // частота, КГц/0(вся запись пустая)/-1(нет данных по частоте)
        public double F_IRI;


        // Пеленг1,2 (Получены от СП2,СП3)
        // в град 0...360 по часовой стрелке/-1(нет данных)
        public double Pel1;
        public double Pel2;

        // Коды модуляции
        //public int[] kod_mod;

        // X(м) координата X на плоскости/0 (нет данных)
        public double X_IRI;
        // Y(м) координата Y на плоскости (север)/0 (нет данных)
        public double Y_IRI;
        // Z(м) координата Z высота/0 (нет данных)
        public double Z_IRI;

        // номер СП1 (от которой пришли данные по частоте)/-1(нет данных)
        public int NSP1;

        // Признак попадания в диапазоны частот(сформированный как результат пересечения
        // литер и диапазонов СП) и углов(+1/-1)
        public int PriznPop;

        // Приоритет для СП1/СП2
        public int Prior1;
        public int Prior2;

        // Номер станции помех(СП), на которую распределили ИРИ
        // и номер канала СП
        public int NSPRP;
        public int NKanRP;

        // 555
        // Попадает в запрещенные частоты по СП1
        public int fl_zapr1;
        // Попадает в запрещенные частоты по СП2
        public int fl_zapr2;

    }
    // ---------------------------------------------------------------------------------------------
    // Для нового массива диапазонов частот и углов с указанием, по какой литере 
    // этот диапазон

    public struct S_FB_DistrIRI
    {
        public double Fmin;
        public double Fmax;
        public double Bmin;
        public double Bmax;
        public int NumLit;
        public int NumKan;
    }
    // ---------------------------------------------------------------------------------------------
    // Для создания листа каналов

    public struct S_Kan_DistrIRI
    {
        // MaxIRILit/0
        public int flKan;
        public int NumLit;
    }
    // ---------------------------------------------------------------------------------------------
    // Для диапазонов частот

    public struct S_F_DistrIRI
    {
        public double Fmin;
        public double Fmax;
    }
    // ---------------------------------------------------------------------------------------------

    // ************************************************************************************* Structs

    public static class ClassTargetDistribution
    {

        // DISTR IRI ********************************************************************************
        // Возврат функции: 
        //     0 -> хоть один ИРИ закрепился за СП
        //    -1 -> ни один ИРИ не прикрепился к СП 
        //    -2 -> нет входных данных


        /*
                public static int f_DistributionIRI_SP(

                                       // БД СП
                                       S_SP_DistrIRI[] mas_SP,

                                       // 555
                                       // Тип станции
                                       //int TypeStation,

                                       // 555
                                       // Запрещенные частоты
                                       List<TableFreqSpec> freqForbidden,

                                       // БД ИРИ
                                       ref S_IRI_DistrIRI[] mas_IRI
                                   )
                {

                    // ----------------------------------------------------------------
                    // Distribution_IRI

                    List<S_FB_DistrIRI> list_SP1_DistrIRI = new List<S_FB_DistrIRI>();
                    List<S_FB_DistrIRI> list_SP2_DistrIRI = new List<S_FB_DistrIRI>();
                    List<S_Kan_DistrIRI> list_KanSP1_DistrIRI = new List<S_Kan_DistrIRI>();
                    List<S_Kan_DistrIRI> list_KanSP2_DistrIRI = new List<S_Kan_DistrIRI>();

                    List<S_F_DistrIRI> list_LIT_DistrIRI = new List<S_F_DistrIRI>();

                    // 555
                    int MaxNumbLit = 10; // Number of liters
                    //if (TypeStation == 0)
                    //    MaxNumbLit = 7;
                    //else MaxNumbLit = 9;
                    // -----------------------------------------------------------------
                    // Количество ИРИ и СП
                    int nIRI = 0;
                    int nSP = 0;
                    int sh_IRI = 0;
                    int sh_SP = 0;
                    int sh_diap = 0;
                    int ndiap = 0;
                    int fl_prod = 0;
                    int sh_lit = 0;
                    double fmin1 = 0;
                    double fmin1_dubl = 0;
                    double fmax1 = 0;
                    int fl_min = 0;
                    int fl_min_dubl = 0;
                    int num_kan = 0;
                    int fl1_SP1 = 0;
                    int fl2_SP2 = 0;

                    // Расстояние между ИРИ и СП (dX=Xири-Xсп...)
                    double dX = 0;
                    double dY = 0;
                    double dZ = 0;
                    double D_IRI_SP1 = 0;
                    double D_IRI_SP2 = 0;

                    int prizn1 = 0;
                    int prizn2 = 0;

                    int indSP = 0;
                    int indKan = 0;
                    int shKan = 0;
                    int Count_IRI = 0;
                    // -------------------------------------------------------------------------

                    // Анализ входной информации **********************************************
                    // -------------------------------------------------------------------------
                    if ((mas_IRI == null) || (mas_SP == null))
                        return -2;
                    // -------------------------------------------------------------------------
                    // Число ИРИ и СП

                    nIRI = mas_IRI.Length;
                    nSP = mas_SP.Length;

                    if ((nIRI == 0) || (nSP == 0))
                        return -2;
                    // ------------------------------------------------------------------------

                    // ********************************************** Анализ входной информации

                    // Литеры *****************************************************************
                    list_LIT_DistrIRI.Clear();

                    S_F_DistrIRI objS_F_DistrIRI = new S_F_DistrIRI();

                    objS_F_DistrIRI.Fmin = 30000;
                    objS_F_DistrIRI.Fmax = 50000;
                    list_LIT_DistrIRI.Add(objS_F_DistrIRI);
                    objS_F_DistrIRI.Fmin = 50000;
                    objS_F_DistrIRI.Fmax = 90000;
                    list_LIT_DistrIRI.Add(objS_F_DistrIRI);
                    objS_F_DistrIRI.Fmin = 90000;
                    objS_F_DistrIRI.Fmax = 160000;
                    list_LIT_DistrIRI.Add(objS_F_DistrIRI);
                    objS_F_DistrIRI.Fmin = 160000;
                    objS_F_DistrIRI.Fmax = 290000;
                    list_LIT_DistrIRI.Add(objS_F_DistrIRI);
                    objS_F_DistrIRI.Fmin = 290000;
                    objS_F_DistrIRI.Fmax = 512000;
                    list_LIT_DistrIRI.Add(objS_F_DistrIRI);
                    objS_F_DistrIRI.Fmin = 512000;
                    objS_F_DistrIRI.Fmax = 860000;
                    list_LIT_DistrIRI.Add(objS_F_DistrIRI);
                    objS_F_DistrIRI.Fmin = 860000;
                    objS_F_DistrIRI.Fmax = 1215000;
                    list_LIT_DistrIRI.Add(objS_F_DistrIRI);
                    objS_F_DistrIRI.Fmin = 1215000;
                    objS_F_DistrIRI.Fmax = 2000000;
                    list_LIT_DistrIRI.Add(objS_F_DistrIRI);
                    objS_F_DistrIRI.Fmin = 2000000;
                    objS_F_DistrIRI.Fmax = 3000000;
                    list_LIT_DistrIRI.Add(objS_F_DistrIRI);
                    objS_F_DistrIRI.Fmin = 3000000;
                    objS_F_DistrIRI.Fmax = 6000000;
                    list_LIT_DistrIRI.Add(objS_F_DistrIRI);


                    // ***************************************************************** Литеры

                    // mas_SP_IN-> mas_SP *****************************************************
                    // Преобразование входного массива для СП по частотам и углам в соответствии
                    // с работой по литерам

                    S_FB_DistrIRI objS_FB_DistrIRI = new S_FB_DistrIRI();
                    S_Kan_DistrIRI objS_Kan_DistrIRI = new S_Kan_DistrIRI();

                    // FOR1
                    for (sh_SP = 0; sh_SP < nSP; sh_SP++)
                    {

                        ndiap = mas_SP[sh_SP].mas_SP_IN.Length;
                        num_kan = 0;
                        //num_lit = 0;
                        // ---------------------------------------------------------------------- 
                        // FOR2 diap

                        for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                        {

                            fl_prod = 0;
                            fl_min = 0;
                            fl_min_dubl = 0;
                            //fl_max = 0;
                            // ....................................................................
                            // FOR3 litera

                            for (sh_lit = 0; sh_lit < MaxNumbLit; sh_lit++)
                            {

                                if (fl_prod == 1) goto vybmax;

                                // fl_min_dubl==1
                                if (fl_min_dubl == 1)
                                {

                                    if (mas_SP[sh_SP].Lit[sh_lit] == 1)   // Litera active
                                    {
                                        fmin1 = fmin1_dubl;
                                        fl_min = 1;
                                        fl_min_dubl = 0;
                                    }
                                    else // Litera NO active
                                    {
                                        if (mas_SP[sh_SP].mas_SP_IN[sh_diap].Fmax > list_LIT_DistrIRI[sh_lit].Fmax)
                                        {
                                            fmin1_dubl = list_LIT_DistrIRI[sh_lit].Fmax;
                                            fl_min = 0;
                                            fl_min_dubl = 1;
                                        }

                                    }

                                } //fl_min_dubl==1


                                // fl_min_dubl=0
                                else
                                {
                                    // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                                    // fmin

                                    // fmin этого диапазона меньше fmin Litera[0]
                                    if (
                                        (sh_lit == 0) &&
                                        (mas_SP[sh_SP].mas_SP_IN[sh_diap].Fmin <= list_LIT_DistrIRI[sh_lit].Fmin)
                                        //(mas_SP[sh_SP].Lit[sh_lit] == 1)
                                        )
                                    {
                                        if (mas_SP[sh_SP].Lit[sh_lit] == 1)   // Litera active
                                        {
                                            fmin1 = list_LIT_DistrIRI[sh_lit].Fmin;
                                            fl_min = 1;
                                            fl_min_dubl = 0;
                                        }
                                        else // Litera NO active
                                        {
                                            if (mas_SP[sh_SP].mas_SP_IN[sh_diap].Fmax > list_LIT_DistrIRI[sh_lit].Fmax)
                                            {
                                                fmin1_dubl = list_LIT_DistrIRI[sh_lit].Fmax;
                                                fl_min = 0;
                                                fl_min_dubl = 1;
                                            }

                                        }

                                    }
                                    // ***
                                    else // Находим литеру,где лежит fmin, если на этой станции эта литера активна
                                    {
                                        if (
                                            (mas_SP[sh_SP].mas_SP_IN[sh_diap].Fmin >= list_LIT_DistrIRI[sh_lit].Fmin) &&
                                            (mas_SP[sh_SP].mas_SP_IN[sh_diap].Fmin < list_LIT_DistrIRI[sh_lit].Fmax)
                                           //(mas_SP[sh_SP].Lit[sh_lit]==1)
                                           )
                                        {
                                            if (mas_SP[sh_SP].Lit[sh_lit] == 1) // Litera active
                                            {
                                                fmin1 = mas_SP[sh_SP].mas_SP_IN[sh_diap].Fmin;
                                                fl_min = 1;
                                                fl_min_dubl = 0;

                                            }
                                            else  // Litera NO active
                                            {
                                                if (mas_SP[sh_SP].mas_SP_IN[sh_diap].Fmax > list_LIT_DistrIRI[sh_lit].Fmax)
                                                {
                                                    fmin1_dubl = list_LIT_DistrIRI[sh_lit].Fmax;
                                                    fl_min = 0;
                                                    fl_min_dubl = 1;
                                                }

                                            }

                                        }

                                    } // else ***

                                } // fl_min_dubl==0
                                  // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                                  // fmax

                                vybmax: if (fl_min == 1)
                                {
                                    // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                                    // Диапазон станции входит в диапазон литеры

                                    if (
                                        (mas_SP[sh_SP].mas_SP_IN[sh_diap].Fmax <= list_LIT_DistrIRI[sh_lit].Fmax)
                                       //(mas_SP[sh_SP].Lit[sh_lit] == 1)
                                       )
                                    {

                                        // Литера неактивна-> переходим к следующему диапазону
                                        if (mas_SP[sh_SP].Lit[sh_lit] == 0)
                                        {
                                            break;
                                        }


                                        fmax1 = mas_SP[sh_SP].mas_SP_IN[sh_diap].Fmax;
                                        //fl_max = 1;
                                        objS_FB_DistrIRI.Fmin = fmin1;
                                        objS_FB_DistrIRI.Fmax = fmax1;
                                        objS_FB_DistrIRI.Bmin = mas_SP[sh_SP].mas_SP_IN[sh_diap].Beta_min;
                                        objS_FB_DistrIRI.Bmax = mas_SP[sh_SP].mas_SP_IN[sh_diap].Beta_max;
                                        objS_FB_DistrIRI.NumLit = sh_lit;
                                        //objS_FB_DistrIRI.NumKan = num_kan;

                                        if (sh_SP == 0)
                                            list_SP1_DistrIRI.Add(objS_FB_DistrIRI);
                                        else
                                            list_SP2_DistrIRI.Add(objS_FB_DistrIRI);


                                        if (((sh_SP == 0) && (fl1_SP1 == 0)) ||
                                            ((sh_SP == 1) && (fl2_SP2 == 0))) // 1-й раз
                                        {
                                            objS_Kan_DistrIRI.flKan = mas_SP[sh_SP].MaxIRILit;
                                            objS_Kan_DistrIRI.NumLit = sh_lit;
                                            num_kan = sh_lit;
                                            if (sh_SP == 0)
                                            {
                                                list_KanSP1_DistrIRI.Add(objS_Kan_DistrIRI);
                                                fl1_SP1 = 1;
                                            }
                                            else
                                            {
                                                list_KanSP2_DistrIRI.Add(objS_Kan_DistrIRI);
                                                fl2_SP2 = 1;
                                            }
                                        }  // 1raz

                                        else
                                        {

                                            objS_Kan_DistrIRI.flKan = mas_SP[sh_SP].MaxIRILit;
                                            objS_Kan_DistrIRI.NumLit = sh_lit;
                                            if (sh_SP == 0)
                                            {
                                                if (list_KanSP1_DistrIRI.FindIndex(x => x.NumLit == sh_lit) < 0)
                                                    list_KanSP1_DistrIRI.Add(objS_Kan_DistrIRI);
                                            }
                                            else
                                            {
                                                if (list_KanSP2_DistrIRI.FindIndex(x => x.NumLit == sh_lit) < 0)
                                                    list_KanSP2_DistrIRI.Add(objS_Kan_DistrIRI);
                                            }


                                        } // NO 1 raz


                                        // Переходим к следующему диапазону
                                        break;

                                    } // Диапазон станции входит в диапазон литеры
                                    // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                                    // fmax диапазона>fmax литеры

                                    else
                                    {
                                        // 1111111111111111111111111111111111111111111111111111111111111111
                                        //if(mas_SP[sh_SP].Lit[sh_lit] == 1)
                                        if (fl_prod == 0)
                                        {

                                            fmax1 = list_LIT_DistrIRI[sh_lit].Fmax;
                                            //fl_max = 1;
                                            objS_FB_DistrIRI.Fmin = fmin1;
                                            objS_FB_DistrIRI.Fmax = fmax1;
                                            objS_FB_DistrIRI.Bmin = mas_SP[sh_SP].mas_SP_IN[sh_diap].Beta_min;
                                            objS_FB_DistrIRI.Bmax = mas_SP[sh_SP].mas_SP_IN[sh_diap].Beta_max;
                                            objS_FB_DistrIRI.NumLit = sh_lit;
                                            //objS_FB_DistrIRI.NumKan = num_kan;
                                            //num_kan++;

                                            if (sh_SP == 0)
                                                list_SP1_DistrIRI.Add(objS_FB_DistrIRI);
                                            else
                                                list_SP2_DistrIRI.Add(objS_FB_DistrIRI);

                                            if (((sh_SP == 0) && (fl1_SP1 == 0)) ||
                                                ((sh_SP == 1) && (fl2_SP2 == 0))) // 1-й раз
                                            {
                                                objS_Kan_DistrIRI.flKan = mas_SP[sh_SP].MaxIRILit;
                                                objS_Kan_DistrIRI.NumLit = sh_lit;
                                                num_kan = sh_lit;
                                                if (sh_SP == 0)
                                                {
                                                    list_KanSP1_DistrIRI.Add(objS_Kan_DistrIRI);
                                                    fl1_SP1 = 1;
                                                }
                                                else
                                                {
                                                    list_KanSP2_DistrIRI.Add(objS_Kan_DistrIRI);
                                                    fl2_SP2 = 1;
                                                }
                                            } // 1raz

                                            else
                                            {
                                                objS_Kan_DistrIRI.flKan = mas_SP[sh_SP].MaxIRILit;
                                                objS_Kan_DistrIRI.NumLit = sh_lit;
                                                if (sh_SP == 0)
                                                {
                                                    if (list_KanSP1_DistrIRI.FindIndex(x => x.NumLit == sh_lit) < 0)
                                                        list_KanSP1_DistrIRI.Add(objS_Kan_DistrIRI);
                                                }
                                                else
                                                {
                                                    if (list_KanSP2_DistrIRI.FindIndex(x => x.NumLit == sh_lit) < 0)
                                                        list_KanSP2_DistrIRI.Add(objS_Kan_DistrIRI);
                                                }

                                            }


                                            fmin1 = list_LIT_DistrIRI[sh_lit].Fmax;
                                            fl_min = 1;
                                            //fl_max = 0;
                                            fl_prod = 1;

                                        } // if(fl_prod==0)
                                        // 1111111111111111111111111111111111111111111111111111111111111111
                                        // fl_prod==1

                                        else
                                        {
                                            // Литера неактивна-> пропустить ее
                                            if (mas_SP[sh_SP].Lit[sh_lit] == 0)
                                            {
                                                fmin1 = list_LIT_DistrIRI[sh_lit].Fmax;
                                                fl_min = 1;
                                                //fl_max = 0;
                                                fl_prod = 1;

                                            }

                                            // Литера активна -> включить полностью этот диапазон
                                            else
                                            {
                                                fmax1 = list_LIT_DistrIRI[sh_lit].Fmax;
                                                //fl_max = 1;
                                                objS_FB_DistrIRI.Fmin = fmin1;
                                                objS_FB_DistrIRI.Fmax = fmax1;
                                                objS_FB_DistrIRI.Bmin = mas_SP[sh_SP].mas_SP_IN[sh_diap].Beta_min;
                                                objS_FB_DistrIRI.Bmax = mas_SP[sh_SP].mas_SP_IN[sh_diap].Beta_max;
                                                objS_FB_DistrIRI.NumLit = sh_lit;
                                                //objS_FB_DistrIRI.NumKan = num_kan;
                                                //num_kan++;

                                                if (sh_SP == 0)
                                                    list_SP1_DistrIRI.Add(objS_FB_DistrIRI);
                                                else
                                                    list_SP2_DistrIRI.Add(objS_FB_DistrIRI);

                                                if (((sh_SP == 0) && (fl1_SP1 == 0)) ||
                                                    ((sh_SP == 1) && (fl2_SP2 == 0))) // 1-й раз
                                                {
                                                    objS_Kan_DistrIRI.flKan = mas_SP[sh_SP].MaxIRILit;
                                                    objS_Kan_DistrIRI.NumLit = sh_lit;
                                                    num_kan = sh_lit;
                                                    if (sh_SP == 0)
                                                    {
                                                        list_KanSP1_DistrIRI.Add(objS_Kan_DistrIRI);
                                                        fl1_SP1 = 1;
                                                    }
                                                    else
                                                    {
                                                        list_KanSP2_DistrIRI.Add(objS_Kan_DistrIRI);
                                                        fl2_SP2 = 1;
                                                    }
                                                } // 1raz

                                                else
                                                {
                                                    objS_Kan_DistrIRI.flKan = mas_SP[sh_SP].MaxIRILit;
                                                    objS_Kan_DistrIRI.NumLit = sh_lit;
                                                    if (sh_SP == 0)
                                                    {
                                                        if (list_KanSP1_DistrIRI.FindIndex(x => x.NumLit == sh_lit) < 0)
                                                            list_KanSP1_DistrIRI.Add(objS_Kan_DistrIRI);
                                                    }
                                                    else
                                                    {
                                                        if (list_KanSP2_DistrIRI.FindIndex(x => x.NumLit == sh_lit) < 0)
                                                            list_KanSP2_DistrIRI.Add(objS_Kan_DistrIRI);
                                                    }

                                                }


                                                fmin1 = list_LIT_DistrIRI[sh_lit].Fmax;
                                                fl_min = 1;
                                                //fl_max = 0;
                                                fl_prod = 1;


                                            } // SP works on this litera

                                        } // fl_prod==1
                                        // 1111111111111111111111111111111111111111111111111111111111111111

                                    } // fmax диапазона>fmax литеры
                                    // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                                } // if(fl_min == 1)
                                // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

                            } // FOR3 Litera
                            // ....................................................................

                        } // FOR2 diap
                        // ---------------------------------------------------------------------- 

                    } // FOR1 SP
                    // ***************************************************** mas_SP_IN-> mas_SP

                    // TEST_DIAP ***************************************************************
                    // Проверка вхождения ИРИ в переформированные диапазоны

                    // FOR5
                    for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
                    {
                        fl_min = 0;
                        // ----------------------------------------------------------------------
                        // SP1 

                        ndiap = list_SP1_DistrIRI.Count;
                        // FOR6
                        for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                        {
                            if (
                                (mas_IRI[sh_IRI].F_IRI >= list_SP1_DistrIRI[sh_diap].Fmin) &&
                                (mas_IRI[sh_IRI].F_IRI <= list_SP1_DistrIRI[sh_diap].Fmax)
                               )
                            {
                                fl_min = 1;
                                sh_diap = ndiap; // Выход из for6
                            }
                        } // FOR6
                        // ---------------------------------------------------------------------- 
                        // SP2

                        if (fl_min == 0)
                        {
                            ndiap = list_SP2_DistrIRI.Count;
                            // FOR7
                            for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                            {
                                if (
                                    (mas_IRI[sh_IRI].F_IRI >= list_SP2_DistrIRI[sh_diap].Fmin) &&
                                    (mas_IRI[sh_IRI].F_IRI <= list_SP2_DistrIRI[sh_diap].Fmax)
                                   )
                                {
                                    fl_min = 1;
                                    sh_diap = ndiap; // Выход из for7
                                }
                            } // FOR7

                        } // if(fl_min == 0)
                        // ---------------------------------------------------------------------- 
                        if (fl_min == 0)
                        {
                            mas_IRI[sh_IRI].PriznPop = -1;
                        }
                        else
                        {
                            mas_IRI[sh_IRI].PriznPop = 0;
                        }
                        // ---------------------------------------------------------------------- 


                    } // FOR5 IRI
                    // *************************************************************** TEST_DIAP

                    // TEST_ZAPR ***************************************************************
                    // Проверка вхождения ИРИ в диапазоны запрещенных частот
                    // 555

                    // 
                    // FOR55
                    for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
                    {

                        // ----------------------------------------------------------------------
                        // SP1 

                        ndiap = freqForbidden.Count;
                        // FOR66
                        for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                        {
                            if (
                                (mas_SP[0].N_SP == freqForbidden[sh_diap].NumberASP) &&
                                (mas_IRI[sh_IRI].F_IRI != -1) &&
                                (mas_IRI[sh_IRI].F_IRI >= freqForbidden[sh_diap].FreqMinKHz) &&
                                (mas_IRI[sh_IRI].F_IRI <= freqForbidden[sh_diap].FreqMaxKHz)
                               )
                            {
                                mas_IRI[sh_IRI].fl_zapr1 = 1;
                                sh_diap = ndiap; // Выход из for66
                            }
                        } // FOR66
                        // ---------------------------------------------------------------------- 
                        // SP2 

                        ndiap = freqForbidden.Count;
                        // FOR77
                        for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                        {
                            if (
                                (mas_SP[1].N_SP == freqForbidden[sh_diap].NumberASP) &&
                                (mas_IRI[sh_IRI].F_IRI != -1) &&
                                (mas_IRI[sh_IRI].F_IRI >= freqForbidden[sh_diap].FreqMinKHz) &&
                                (mas_IRI[sh_IRI].F_IRI <= freqForbidden[sh_diap].FreqMaxKHz)
                               )
                            {
                                mas_IRI[sh_IRI].fl_zapr2 = 1;
                                sh_diap = ndiap; // Выход из for77
                            }
                        } // FOR77
                        // ---------------------------------------------------------------------- 

                    } // FOR55 IRI
                    // *************************************************************** TEST_ZAPR

                    // PRIOR IRI XYZ ***********************************************************
                    // Приоритеты ИРИ с Координатами

                    // FOR9
                    for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
                    {

                        // IF (есть F и координаты)
                        if (
                            (mas_IRI[sh_IRI].F_IRI != 0) &&
                            (mas_IRI[sh_IRI].F_IRI != -1) &&
                            (mas_IRI[sh_IRI].X_IRI != 0) &&
                            (mas_IRI[sh_IRI].Y_IRI != 0) &&
                            //(mas_IRI[sh_IRI].Z_IRI != 0)&&
                            (mas_IRI[sh_IRI].PriznPop != -1)
                           )
                        {

                            prizn1 = 0;
                            prizn2 = 0;
                            // ----------------------------------------------------------------------
                            // SP1 

                            ndiap = list_SP1_DistrIRI.Count;
                            // FOR10
                            for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                            {
                                if (
                                    (mas_IRI[sh_IRI].F_IRI >= list_SP1_DistrIRI[sh_diap].Fmin) &&
                                    (mas_IRI[sh_IRI].F_IRI <= list_SP1_DistrIRI[sh_diap].Fmax) &&
                                    (mas_IRI[sh_IRI].Pel1 >= list_SP1_DistrIRI[sh_diap].Bmin) &&
                                    (mas_IRI[sh_IRI].Pel1 <= list_SP1_DistrIRI[sh_diap].Bmax)
                                   )
                                {
                                    prizn1 = 1;
                                    sh_diap = ndiap; // Выход из for
                                }
                            } // FOR10
                            // ---------------------------------------------------------------------- 
                            // SP2

                            ndiap = list_SP2_DistrIRI.Count;
                            // FOR11
                            for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                            {
                                if (
                                    (mas_IRI[sh_IRI].F_IRI >= list_SP2_DistrIRI[sh_diap].Fmin) &&
                                    (mas_IRI[sh_IRI].F_IRI <= list_SP2_DistrIRI[sh_diap].Fmax) &&
                                    (mas_IRI[sh_IRI].Pel2 >= list_SP2_DistrIRI[sh_diap].Bmin) &&
                                    (mas_IRI[sh_IRI].Pel2 <= list_SP2_DistrIRI[sh_diap].Bmax)
                                   )
                                {
                                    prizn2 = 1;
                                    sh_diap = ndiap; // Выход из for
                                }
                            } // FOR11
                            // ---------------------------------------------------------------------- 
                            // Есть попадание в обе СП

                            if ((prizn1 == 1) && (prizn2 == 1))
                            {
                                // .....................................................................
                                // SP1

                                dX = mas_IRI[sh_IRI].X_IRI - mas_SP[0].X_SP;
                                dY = mas_IRI[sh_IRI].Y_IRI - mas_SP[0].Y_SP;
                                dZ = mas_IRI[sh_IRI].Z_IRI - mas_SP[0].Z_SP;
                                D_IRI_SP1 = Math.Sqrt(dX * dX + dY * dY + dZ * dZ);
                                // .....................................................................
                                // SP2

                                dX = mas_IRI[sh_IRI].X_IRI - mas_SP[1].X_SP;
                                dY = mas_IRI[sh_IRI].Y_IRI - mas_SP[1].Y_SP;
                                dZ = mas_IRI[sh_IRI].Z_IRI - mas_SP[1].Z_SP;
                                D_IRI_SP2 = Math.Sqrt(dX * dX + dY * dY + dZ * dZ);
                                // .....................................................................
                                if (D_IRI_SP1 <= D_IRI_SP2)
                                {
                                    // 555
                                    //mas_IRI[sh_IRI].Prior1 = 4;
                                    //mas_IRI[sh_IRI].Prior2 = 3;

                                    // 555
                                    // Попал в запрещенные частоты по двум станциям
                                    if ((mas_IRI[sh_IRI].fl_zapr1 == 1) && (mas_IRI[sh_IRI].fl_zapr2 == 1))
                                    {
                                        mas_IRI[sh_IRI].PriznPop = -1;
                                    }
                                    else if ((mas_IRI[sh_IRI].fl_zapr1 == 1) && (mas_IRI[sh_IRI].fl_zapr2 == 0))
                                    {
                                        mas_IRI[sh_IRI].Prior1 = 0;
                                        mas_IRI[sh_IRI].Prior2 = 4;
                                    }
                                    else if ((mas_IRI[sh_IRI].fl_zapr1 == 0) && (mas_IRI[sh_IRI].fl_zapr2 == 1))
                                    {
                                        mas_IRI[sh_IRI].Prior1 = 4;
                                        mas_IRI[sh_IRI].Prior2 = 0;
                                    }
                                    else
                                    {
                                        mas_IRI[sh_IRI].Prior1 = 4;
                                        mas_IRI[sh_IRI].Prior2 = 3;
                                    }

                                } // if (D_IRI_SP1 <= D_IRI_SP2)

                                else
                                {
                                    // 555
                                    //mas_IRI[sh_IRI].Prior1 = 3;
                                    //mas_IRI[sh_IRI].Prior2 = 4;

                                    // 555
                                    // Попал в запрещенные частоты по двум станциям
                                    if ((mas_IRI[sh_IRI].fl_zapr1 == 1) && (mas_IRI[sh_IRI].fl_zapr2 == 1))
                                    {
                                        mas_IRI[sh_IRI].PriznPop = -1;
                                    }
                                    else if ((mas_IRI[sh_IRI].fl_zapr1 == 1) && (mas_IRI[sh_IRI].fl_zapr2 == 0))
                                    {
                                        mas_IRI[sh_IRI].Prior1 = 0;
                                        mas_IRI[sh_IRI].Prior2 = 4;
                                    }
                                    else if ((mas_IRI[sh_IRI].fl_zapr1 == 0) && (mas_IRI[sh_IRI].fl_zapr2 == 1))
                                    {
                                        mas_IRI[sh_IRI].Prior1 = 4;
                                        mas_IRI[sh_IRI].Prior2 = 0;
                                    }
                                    else
                                    {
                                        mas_IRI[sh_IRI].Prior1 = 3;
                                        mas_IRI[sh_IRI].Prior2 = 4;
                                    }

                                } //// if (D_IRI_SP1 > D_IRI_SP2)

                                // .....................................................................
                            }  // if((prizn1==1)&&(prizn2==1))
                            // ---------------------------------------------------------------------- 
                            else if ((prizn1 == 1) && (prizn2 == 0))
                            {
                                // Не попал в диапазоны по СП2
                                mas_IRI[sh_IRI].Prior2 = 0;

                                // 555
                                // Попал в запрещенные частоты по СП1
                                if (mas_IRI[sh_IRI].fl_zapr1 == 1)
                                    mas_IRI[sh_IRI].PriznPop = -1;
                                else
                                    mas_IRI[sh_IRI].Prior1 = 4;

                            } // (prizn1 == 1) && (prizn2 == 0)
                            // ---------------------------------------------------------------------- 
                            else if ((prizn1 == 0) && (prizn2 == 1))
                            {
                                // Не попал в диапазоны по СП1
                                mas_IRI[sh_IRI].Prior1 = 0;

                                // 555
                                // Попал в запрещенные частоты по СП2
                                if (mas_IRI[sh_IRI].fl_zapr2 == 1)
                                    mas_IRI[sh_IRI].PriznPop = -1;
                                else
                                    mas_IRI[sh_IRI].Prior2 = 4;

                            } // (prizn1 == 0) && (prizn2 == 1)
                            // ---------------------------------------------------------------------- 
                            else
                            {
                                // Не попал в диапазоны ни по одной станции
                                mas_IRI[sh_IRI].PriznPop = -1;

                            } // (prizn1 == 0) && (prizn2 == 0)
                            // ---------------------------------------------------------------------- 

                        } // IF (есть F и координаты)

                    } // FOR9 IRI
                    // *********************************************************** PRIOR IRI XYZ

                    // PRIOR IRI  **************************************************************
                    // Приоритеты ИРИ без Координат

                    // FOR12
                    for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
                    {

                        // IF (есть F NO coordinates)
                        if (
                            (mas_IRI[sh_IRI].F_IRI != 0) &&
                            (mas_IRI[sh_IRI].F_IRI != -1) &&
                            (mas_IRI[sh_IRI].X_IRI == 0) &&
                            (mas_IRI[sh_IRI].Y_IRI == 0) &&
                            (mas_IRI[sh_IRI].Z_IRI == 0) &&
                            (mas_IRI[sh_IRI].PriznPop != -1)
                           )
                        {

                            // ----------------------------------------------------------------------
                            // Информация пришла от СП1 

                            // 555
                            //if (mas_IRI[sh_IRI].NSP1 == 1)
                            if (mas_IRI[sh_IRI].NSP1 == mas_SP[0].N_SP)
                            {
                                prizn1 = 0;
                                prizn2 = 0;

                                // Проверить СП1
                                ndiap = list_SP1_DistrIRI.Count;
                                // FOR
                                for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                                {
                                    if (
                                        (mas_IRI[sh_IRI].F_IRI >= list_SP1_DistrIRI[sh_diap].Fmin) &&
                                        (mas_IRI[sh_IRI].F_IRI <= list_SP1_DistrIRI[sh_diap].Fmax) &&
                                        (mas_IRI[sh_IRI].Pel1 >= list_SP1_DistrIRI[sh_diap].Bmin) &&
                                        (mas_IRI[sh_IRI].Pel1 <= list_SP1_DistrIRI[sh_diap].Bmax)
                                       )
                                    {
                                        prizn1 = 1;
                                        sh_diap = ndiap; // Выход из for
                                    }
                                } // FOR

                                // Проверить СП2
                                ndiap = list_SP2_DistrIRI.Count;
                                // FOR
                                for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                                {
                                    if (
                                        (mas_IRI[sh_IRI].F_IRI >= list_SP2_DistrIRI[sh_diap].Fmin) &&
                                        (mas_IRI[sh_IRI].F_IRI <= list_SP2_DistrIRI[sh_diap].Fmax)
                                       //(mas_IRI[sh_IRI].Pel1 >= list_SP1_DistrIRI[sh_diap].Bmin) && // ??????????????
                                       //(mas_IRI[sh_IRI].Pel1 <= list_SP1_DistrIRI[sh_diap].Bmax)
                                       )
                                    {
                                        prizn2 = 1;
                                        sh_diap = ndiap; // Выход из for
                                    }
                                } // FOR

                                if ((prizn1 == 1) && (prizn2 == 1))
                                {
                                    // 555
                                    //mas_IRI[sh_IRI].Prior1 = 2;
                                    //mas_IRI[sh_IRI].Prior2 = 1;

                                    // 555
                                    // Попал в запрещенные частоты по двум станциям
                                    if ((mas_IRI[sh_IRI].fl_zapr1 == 1) && (mas_IRI[sh_IRI].fl_zapr2 == 1))
                                    {
                                        mas_IRI[sh_IRI].PriznPop = -1;
                                    }
                                    else if ((mas_IRI[sh_IRI].fl_zapr1 == 1) && (mas_IRI[sh_IRI].fl_zapr2 == 0))
                                    {
                                        mas_IRI[sh_IRI].Prior1 = 0;
                                        mas_IRI[sh_IRI].Prior2 = 1;
                                    }
                                    else if ((mas_IRI[sh_IRI].fl_zapr1 == 0) && (mas_IRI[sh_IRI].fl_zapr2 == 1))
                                    {
                                        mas_IRI[sh_IRI].Prior1 = 2;
                                        mas_IRI[sh_IRI].Prior2 = 0;
                                    }
                                    else
                                    {
                                        mas_IRI[sh_IRI].Prior1 = 2;
                                        mas_IRI[sh_IRI].Prior2 = 1;
                                    }

                                }
                                else if ((prizn1 == 0) && (prizn2 == 1))
                                {
                                    mas_IRI[sh_IRI].Prior1 = 0;

                                    // 555
                                    // Попал в запрещенные частоты по СП2
                                    if (mas_IRI[sh_IRI].fl_zapr2 == 1)
                                        mas_IRI[sh_IRI].PriznPop = -1;
                                    else
                                        mas_IRI[sh_IRI].Prior2 = 1; // ????
                                }

                                else if ((prizn1 == 1) && (prizn2 == 0))
                                {
                                    mas_IRI[sh_IRI].Prior2 = 0;

                                    // 555
                                    // Попал в запрещенные частоты по СП1
                                    if (mas_IRI[sh_IRI].fl_zapr1 == 1)
                                        mas_IRI[sh_IRI].PriznPop = -1;
                                    else
                                        mas_IRI[sh_IRI].Prior1 = 2;
                                }

                                else
                                {
                                    mas_IRI[sh_IRI].PriznPop = -1;
                                }


                            } // Информация пришла от СП1
                            // ---------------------------------------------------------------------- 
                            // Информация пришла от СП2 

                            // 555
                            //if (mas_IRI[sh_IRI].NSP1 == 2)
                            if (mas_IRI[sh_IRI].NSP1 == mas_SP[1].N_SP)
                            {
                                prizn1 = 0;
                                prizn2 = 0;

                                // Проверить СП1
                                ndiap = list_SP1_DistrIRI.Count;
                                // FOR
                                for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                                {
                                    if (
                                        (mas_IRI[sh_IRI].F_IRI >= list_SP1_DistrIRI[sh_diap].Fmin) &&
                                        (mas_IRI[sh_IRI].F_IRI <= list_SP1_DistrIRI[sh_diap].Fmax)
                                       //(mas_IRI[sh_IRI].Pel1 >= list_SP1_DistrIRI[sh_diap].Bmin) &&
                                       //(mas_IRI[sh_IRI].Pel1 <= list_SP1_DistrIRI[sh_diap].Bmax)
                                       )
                                    {
                                        prizn1 = 1;
                                        sh_diap = ndiap; // Выход из for
                                    }
                                } // FOR

                                // Проверить СП2
                                ndiap = list_SP2_DistrIRI.Count;
                                // FOR
                                for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                                {
                                    if (
                                        (mas_IRI[sh_IRI].F_IRI >= list_SP2_DistrIRI[sh_diap].Fmin) &&
                                        (mas_IRI[sh_IRI].F_IRI <= list_SP2_DistrIRI[sh_diap].Fmax) &&
                                        (mas_IRI[sh_IRI].Pel1 >= list_SP2_DistrIRI[sh_diap].Bmin) &&
                                        (mas_IRI[sh_IRI].Pel1 <= list_SP2_DistrIRI[sh_diap].Bmax)
                                       )
                                    {
                                        prizn2 = 1;
                                        sh_diap = ndiap; // Выход из for
                                    }
                                } // FOR

                                if ((prizn1 == 1) && (prizn2 == 1))
                                {
                                    //mas_IRI[sh_IRI].Prior1 = 1;
                                    //mas_IRI[sh_IRI].Prior2 = 2;

                                    // 555
                                    // Попал в запрещенные частоты по двум станциям
                                    if ((mas_IRI[sh_IRI].fl_zapr1 == 1) && (mas_IRI[sh_IRI].fl_zapr2 == 1))
                                    {
                                        mas_IRI[sh_IRI].PriznPop = -1;
                                    }
                                    else if ((mas_IRI[sh_IRI].fl_zapr1 == 1) && (mas_IRI[sh_IRI].fl_zapr2 == 0))
                                    {
                                        mas_IRI[sh_IRI].Prior1 = 0;
                                        mas_IRI[sh_IRI].Prior2 = 2;
                                    }
                                    else if ((mas_IRI[sh_IRI].fl_zapr1 == 0) && (mas_IRI[sh_IRI].fl_zapr2 == 1))
                                    {
                                        mas_IRI[sh_IRI].Prior1 = 1;
                                        mas_IRI[sh_IRI].Prior2 = 0;
                                    }
                                    else
                                    {
                                        mas_IRI[sh_IRI].Prior1 = 1;
                                        mas_IRI[sh_IRI].Prior2 = 2;
                                    }

                                }
                                else if ((prizn1 == 0) && (prizn2 == 1))
                                {
                                    mas_IRI[sh_IRI].Prior1 = 0;

                                    // 555
                                    // Попал в запрещенные частоты по СП2
                                    if (mas_IRI[sh_IRI].fl_zapr2 == 1)
                                        mas_IRI[sh_IRI].PriznPop = -1;
                                    else
                                        mas_IRI[sh_IRI].Prior2 = 2;
                                }

                                else if ((prizn1 == 1) && (prizn2 == 0))
                                {
                                    mas_IRI[sh_IRI].Prior2 = 0;

                                    // 555
                                    // Попал в запрещенные частоты по СП1
                                    if (mas_IRI[sh_IRI].fl_zapr1 == 1)
                                        mas_IRI[sh_IRI].PriznPop = -1;
                                    else
                                        mas_IRI[sh_IRI].Prior1 = 1; // ????
                                }

                                else
                                {
                                    mas_IRI[sh_IRI].PriznPop = -1;
                                }

                            } // Информация пришла от СП2
                            // ---------------------------------------------------------------------- 

                        } // IF  (есть F NO coordinates)

                    } // FOR12 IRI
                    // ************************************************************** PRIOR IRI 


                    // !!!RASPREDELENIE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                    Count_IRI = 0;

                    // SP1_4 ******************************************************************
                    // Распределяем на СП1 все подходящие по интервалам частот и углов ИРИ с
                    // приоритетом 4 для СП1

                    // FOR13
                    for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
                    {
                        // IF (PriznPop!=-1&&Prior1==4)
                        if (
                            (mas_IRI[sh_IRI].PriznPop != -1) &&
                            (mas_IRI[sh_IRI].Prior1 == 4)
                           )
                        {

                            // Найти нужный диапазон в СП1
                            ndiap = list_SP1_DistrIRI.Count;
                            // FOR14
                            for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                            {
                                if (
                                    (mas_IRI[sh_IRI].F_IRI >= list_SP1_DistrIRI[sh_diap].Fmin) &&
                                    (mas_IRI[sh_IRI].F_IRI <= list_SP1_DistrIRI[sh_diap].Fmax) &&
                                    (mas_IRI[sh_IRI].Pel1 >= list_SP1_DistrIRI[sh_diap].Bmin) &&
                                    (mas_IRI[sh_IRI].Pel1 <= list_SP1_DistrIRI[sh_diap].Bmax)
                                   )
                                {
                                    // -------------------------------------------------------
                                    // Индекс нужного диапазона

                                    indSP = sh_diap;
                                    // -------------------------------------------------------
                                    // Ищем места по этой литере

                                    // FOR15
                                    for (shKan = 0; shKan < list_KanSP1_DistrIRI.Count; shKan++)
                                    {
                                        if (
                                            (list_KanSP1_DistrIRI[shKan].NumLit == list_SP1_DistrIRI[indSP].NumLit) &&
                                            (list_KanSP1_DistrIRI[shKan].flKan != 0)
                                          )
                                        {
                                            indKan = shKan;
                                            shKan = list_KanSP1_DistrIRI.Count; // For OOUT from FOR

                                            // Уменьшаем число мест по литере
                                            num_kan = list_KanSP1_DistrIRI[indKan].flKan;
                                            num_kan -= 1;
                                            objS_Kan_DistrIRI.flKan = num_kan;
                                            num_kan = list_KanSP1_DistrIRI[indKan].NumLit;
                                            objS_Kan_DistrIRI.NumLit = num_kan;
                                            list_KanSP1_DistrIRI.RemoveAt(indKan);
                                            list_KanSP1_DistrIRI.Insert(indKan, objS_Kan_DistrIRI);

                                            // Присваиваем этот ИРИ 1-й станции
                                            mas_IRI[sh_IRI].NSPRP = mas_SP[0].N_SP;
                                            mas_IRI[sh_IRI].NKanRP = list_KanSP1_DistrIRI[indKan].NumLit; // Litera
                                            mas_IRI[sh_IRI].PriznPop = 1;
                                            Count_IRI++;

                                        } // Litera is free

                                    } // FOR15 Kan
                                    // -------------------------------------------------------
                                    if (mas_IRI[sh_IRI].PriznPop == 1) // ИРИ распределился
                                        sh_diap = ndiap; // Выход из for

                                }

                            } // FOR 14 Diapazon


                        } // IF (PriznPop!=-1&&Prior1==4)


                    } // FOR 13 IRI
                    // ****************************************************************** SP1_4

                    // SP2_4 ******************************************************************
                    // Распределяем на СП2 все подходящие по интервалам частот и углов ИРИ с
                    // приоритетом 4 для СП2

                    // FOR16
                    for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
                    {
                        // IF (PriznPop!=-1&&Prior2==4&&PriznPop!=1)
                        if (
                            (mas_IRI[sh_IRI].PriznPop != -1) &&
                            (mas_IRI[sh_IRI].PriznPop != 1) &&  // ИРИ распределился
                            (mas_IRI[sh_IRI].Prior2 == 4)
                           )
                        {

                            // Найти нужный диапазон в СП2
                            ndiap = list_SP2_DistrIRI.Count;
                            // FOR17
                            for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                            {
                                if (
                                    (mas_IRI[sh_IRI].F_IRI >= list_SP2_DistrIRI[sh_diap].Fmin) &&
                                    (mas_IRI[sh_IRI].F_IRI <= list_SP2_DistrIRI[sh_diap].Fmax) &&
                                    (mas_IRI[sh_IRI].Pel2 >= list_SP2_DistrIRI[sh_diap].Bmin) &&
                                    (mas_IRI[sh_IRI].Pel2 <= list_SP2_DistrIRI[sh_diap].Bmax)
                                   )
                                {
                                    // -------------------------------------------------------
                                    // Индекс нужного диапазона

                                    indSP = sh_diap;
                                    // -------------------------------------------------------
                                    // Ищем места по этой литере

                                    // FOR18
                                    for (shKan = 0; shKan < list_KanSP2_DistrIRI.Count; shKan++)
                                    {
                                        if (
                                            (list_KanSP2_DistrIRI[shKan].NumLit == list_SP2_DistrIRI[indSP].NumLit) &&
                                            (list_KanSP2_DistrIRI[shKan].flKan != 0)
                                          )
                                        {
                                            indKan = shKan;
                                            shKan = list_KanSP2_DistrIRI.Count; // For OOUT from FOR

                                            // Уменьшаем число мест по литере
                                            num_kan = list_KanSP2_DistrIRI[indKan].flKan;
                                            num_kan -= 1;
                                            objS_Kan_DistrIRI.flKan = num_kan;
                                            num_kan = list_KanSP2_DistrIRI[indKan].NumLit;
                                            objS_Kan_DistrIRI.NumLit = num_kan;
                                            list_KanSP2_DistrIRI.RemoveAt(indKan);
                                            list_KanSP2_DistrIRI.Insert(indKan, objS_Kan_DistrIRI);

                                            // Присваиваем этот ИРИ 2-й станции
                                            mas_IRI[sh_IRI].NSPRP = mas_SP[1].N_SP;
                                            mas_IRI[sh_IRI].NKanRP = list_KanSP2_DistrIRI[indKan].NumLit; // Litera
                                            mas_IRI[sh_IRI].PriznPop = 1;
                                            Count_IRI++;

                                        } // Litera is free

                                    } // FOR18 Kan
                                    // -------------------------------------------------------
                                    if (mas_IRI[sh_IRI].PriznPop == 1) // ИРИ распределился
                                        sh_diap = ndiap; // Выход из for

                                }

                            } // FOR 17 Diapazon


                        } // IF (PriznPop!=-1&&Prior2==4&&PriznPop!=1)


                    } // FOR 16 IRI
                    // ****************************************************************** SP2_4

                    // SP1(4)->SP2(3) *******************************************************
                    // Если остались ИРИ с приоритетом 4 для СП1, пытаемся перебросить их на 
                    // СП2 (если по ней у них приоритет 3)

                    // FOR19
                    for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
                    {
                        // IF (PriznPop!=-1&&Prior1==4&&PriznPop!=1)
                        if (
                            (mas_IRI[sh_IRI].PriznPop != -1) &&
                            (mas_IRI[sh_IRI].PriznPop != 1) &&  // ИРИ распределился
                            (mas_IRI[sh_IRI].Prior1 == 4)
                           )
                        {
                            // ---------------------------------------------------------------            
                            if (mas_IRI[sh_IRI].Prior2 == 0)
                                mas_IRI[sh_IRI].PriznPop = -1;
                            // ---------------------------------------------------------------            
                            else if (mas_IRI[sh_IRI].Prior2 == 3)
                            {

                                // Найти нужный диапазон в СП2
                                ndiap = list_SP2_DistrIRI.Count;
                                // FOR20
                                for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                                {
                                    if (
                                        (mas_IRI[sh_IRI].F_IRI >= list_SP2_DistrIRI[sh_diap].Fmin) &&
                                        (mas_IRI[sh_IRI].F_IRI <= list_SP2_DistrIRI[sh_diap].Fmax) &&
                                        (mas_IRI[sh_IRI].Pel2 >= list_SP2_DistrIRI[sh_diap].Bmin) &&
                                        (mas_IRI[sh_IRI].Pel2 <= list_SP2_DistrIRI[sh_diap].Bmax)
                                       )
                                    {
                                        // .........................................................
                                        // Индекс нужного диапазона

                                        indSP = sh_diap;
                                        // .........................................................
                                        // Ищем места по этой литере

                                        // FOR21
                                        for (shKan = 0; shKan < list_KanSP2_DistrIRI.Count; shKan++)
                                        {
                                            if (
                                                (list_KanSP2_DistrIRI[shKan].NumLit == list_SP2_DistrIRI[indSP].NumLit) &&
                                                (list_KanSP2_DistrIRI[shKan].flKan != 0)
                                              )
                                            {
                                                indKan = shKan;
                                                shKan = list_KanSP2_DistrIRI.Count; // For OOUT from FOR

                                                // Уменьшаем число мест по литере
                                                num_kan = list_KanSP2_DistrIRI[indKan].flKan;
                                                num_kan -= 1;
                                                objS_Kan_DistrIRI.flKan = num_kan;
                                                num_kan = list_KanSP2_DistrIRI[indKan].NumLit;
                                                objS_Kan_DistrIRI.NumLit = num_kan;
                                                list_KanSP2_DistrIRI.RemoveAt(indKan);
                                                list_KanSP2_DistrIRI.Insert(indKan, objS_Kan_DistrIRI);

                                                // Присваиваем этот ИРИ 2-й станции
                                                mas_IRI[sh_IRI].NSPRP = mas_SP[1].N_SP;
                                                mas_IRI[sh_IRI].NKanRP = list_KanSP2_DistrIRI[indKan].NumLit; // Litera
                                                mas_IRI[sh_IRI].PriznPop = 1;
                                                Count_IRI++;

                                            } // Litera is free

                                        } // FOR21 Kan
                                        // .........................................................
                                        if (mas_IRI[sh_IRI].PriznPop == 1) // ИРИ распределился
                                            sh_diap = ndiap; // Выход из for
                                        else
                                        {
                                            sh_diap = ndiap;
                                            mas_IRI[sh_IRI].PriznPop = -1;
                                        }

                                    } // IF(попал в диапазон)

                                } // FOR 20 Diapazon

                            } // IF(Prior2==3)
                            // ---------------------------------------------------------------            

                        } // IF (PriznPop!=-1&&Prior1==4&&PriznPop!=1)

                    } // FOR 19 IRI
                    // ******************************************************** SP1(4)->SP2(3)

                    // SP2(4)->SP1(3) *******************************************************
                    // Если остались ИРИ с приоритетом 4 для СП2, пытаемся перебросить их на 
                    // СП1 (если по ней у них приоритет 3)

                    // FOR22
                    for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
                    {
                        // IF (PriznPop!=-1&&Prior2==4&&PriznPop!=1)
                        if (
                            (mas_IRI[sh_IRI].PriznPop != -1) &&
                            (mas_IRI[sh_IRI].PriznPop != 1) &&  // ИРИ распределился
                            (mas_IRI[sh_IRI].Prior2 == 4)
                           )
                        {
                            // ---------------------------------------------------------------            
                            if (mas_IRI[sh_IRI].Prior1 == 0)
                                mas_IRI[sh_IRI].PriznPop = -1;
                            // ---------------------------------------------------------------            
                            else if (mas_IRI[sh_IRI].Prior1 == 3)
                            {

                                // Найти нужный диапазон в СП1
                                ndiap = list_SP1_DistrIRI.Count;
                                // FOR23
                                for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                                {
                                    if (
                                        (mas_IRI[sh_IRI].F_IRI >= list_SP1_DistrIRI[sh_diap].Fmin) &&
                                        (mas_IRI[sh_IRI].F_IRI <= list_SP1_DistrIRI[sh_diap].Fmax) &&
                                        (mas_IRI[sh_IRI].Pel1 >= list_SP1_DistrIRI[sh_diap].Bmin) &&
                                        (mas_IRI[sh_IRI].Pel1 <= list_SP1_DistrIRI[sh_diap].Bmax)
                                       )
                                    {
                                        // .........................................................
                                        // Индекс нужного диапазона

                                        indSP = sh_diap;
                                        // .........................................................
                                        // Ищем места по этой литере

                                        // FOR24
                                        for (shKan = 0; shKan < list_KanSP1_DistrIRI.Count; shKan++)
                                        {
                                            if (
                                                (list_KanSP1_DistrIRI[shKan].NumLit == list_SP1_DistrIRI[indSP].NumLit) &&
                                                (list_KanSP1_DistrIRI[shKan].flKan != 0)
                                              )
                                            {
                                                indKan = shKan;
                                                shKan = list_KanSP1_DistrIRI.Count; // For OOUT from FOR

                                                // Уменьшаем число мест по литере
                                                num_kan = list_KanSP1_DistrIRI[indKan].flKan;
                                                num_kan -= 1;
                                                objS_Kan_DistrIRI.flKan = num_kan;
                                                num_kan = list_KanSP1_DistrIRI[indKan].NumLit;
                                                objS_Kan_DistrIRI.NumLit = num_kan;
                                                list_KanSP1_DistrIRI.RemoveAt(indKan);
                                                list_KanSP1_DistrIRI.Insert(indKan, objS_Kan_DistrIRI);

                                                // Присваиваем этот ИРИ 1-й станции
                                                mas_IRI[sh_IRI].NSPRP = mas_SP[0].N_SP;
                                                mas_IRI[sh_IRI].NKanRP = list_KanSP1_DistrIRI[indKan].NumLit; // Litera
                                                mas_IRI[sh_IRI].PriznPop = 1;
                                                Count_IRI++;

                                            } // Litera is free

                                        } // FOR24 Kan
                                        // .........................................................
                                        if (mas_IRI[sh_IRI].PriznPop == 1) // ИРИ распределился
                                            sh_diap = ndiap; // Выход из for
                                        else
                                        {
                                            sh_diap = ndiap;
                                            mas_IRI[sh_IRI].PriznPop = -1;
                                        }

                                    } // IF(попал в диапазон)

                                } // FOR 23 Diapazon

                            } // IF(Prior1==3)
                            // ---------------------------------------------------------------            

                        } // IF (PriznPop!=-1&&Prior2==4&&PriznPop!=1)

                    } // FOR 22 IRI
                    // ******************************************************** SP2(4)->SP1(3)

                    // SP1_2 ******************************************************************
                    // Распределяем на СП1 все подходящие по интервалам частот и углов ИРИ с
                    // приоритетом 2 для СП1

                    // FOR25
                    for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
                    {
                        // IF (PriznPop!=-1&&Prior1==2&&PriznPop!=1)
                        if (
                            (mas_IRI[sh_IRI].PriznPop != -1) &&
                            (mas_IRI[sh_IRI].PriznPop != 1) &&  // ИРИ распределился
                            (mas_IRI[sh_IRI].Prior1 == 2)
                           )
                        {

                            // Найти нужный диапазон в СП1
                            ndiap = list_SP1_DistrIRI.Count;
                            // FOR26
                            for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                            {
                                if (
                                    (mas_IRI[sh_IRI].F_IRI >= list_SP1_DistrIRI[sh_diap].Fmin) &&
                                    (mas_IRI[sh_IRI].F_IRI <= list_SP1_DistrIRI[sh_diap].Fmax) &&
                                    (mas_IRI[sh_IRI].Pel1 >= list_SP1_DistrIRI[sh_diap].Bmin) &&
                                    (mas_IRI[sh_IRI].Pel1 <= list_SP1_DistrIRI[sh_diap].Bmax)
                                   )
                                {
                                    // -------------------------------------------------------
                                    // Индекс нужного диапазона

                                    indSP = sh_diap;
                                    // -------------------------------------------------------
                                    // Ищем места по этой литере

                                    // FOR27
                                    for (shKan = 0; shKan < list_KanSP1_DistrIRI.Count; shKan++)
                                    {
                                        if (
                                            (list_KanSP1_DistrIRI[shKan].NumLit == list_SP1_DistrIRI[indSP].NumLit) &&
                                            (list_KanSP1_DistrIRI[shKan].flKan != 0)
                                          )
                                        {
                                            indKan = shKan;
                                            shKan = list_KanSP1_DistrIRI.Count; // For OOUT from FOR

                                            // Уменьшаем число мест по литере
                                            num_kan = list_KanSP1_DistrIRI[indKan].flKan;
                                            num_kan -= 1;
                                            objS_Kan_DistrIRI.flKan = num_kan;
                                            num_kan = list_KanSP1_DistrIRI[indKan].NumLit;
                                            objS_Kan_DistrIRI.NumLit = num_kan;
                                            list_KanSP1_DistrIRI.RemoveAt(indKan);
                                            list_KanSP1_DistrIRI.Insert(indKan, objS_Kan_DistrIRI);

                                            // Присваиваем этот ИРИ 1-й станции
                                            mas_IRI[sh_IRI].NSPRP = mas_SP[0].N_SP;
                                            mas_IRI[sh_IRI].NKanRP = list_KanSP1_DistrIRI[indKan].NumLit; // Litera
                                            mas_IRI[sh_IRI].PriznPop = 1;
                                            Count_IRI++;

                                        } // Litera is free

                                    } // FOR27 Kan
                                    // -------------------------------------------------------
                                    if (mas_IRI[sh_IRI].PriznPop == 1) // ИРИ распределился
                                        sh_diap = ndiap; // Выход из for

                                }

                            } // FOR 26 Diapazon


                        } // IF (PriznPop!=-1&&Prior1==2PriznPop!=1)


                    } // FOR 25 IRI
                    // ****************************************************************** SP1_2

                    // SP2_2 ******************************************************************
                    // Распределяем на СП2 все подходящие по интервалам частот и углов ИРИ с
                    // приоритетом 2 для СП2

                    // FOR28
                    for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
                    {
                        // IF (PriznPop!=-1&&Prior2==2&&PriznPop!=1)
                        if (
                            (mas_IRI[sh_IRI].PriznPop != -1) &&
                            (mas_IRI[sh_IRI].PriznPop != 1) &&  // ИРИ распределился
                            (mas_IRI[sh_IRI].Prior2 == 2)
                           )
                        {

                            // Найти нужный диапазон в СП2
                            ndiap = list_SP2_DistrIRI.Count;
                            // FOR29
                            for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                            {
                                if (
                                    (mas_IRI[sh_IRI].F_IRI >= list_SP2_DistrIRI[sh_diap].Fmin) &&
                                    (mas_IRI[sh_IRI].F_IRI <= list_SP2_DistrIRI[sh_diap].Fmax) &&
                                    (mas_IRI[sh_IRI].Pel1 >= list_SP2_DistrIRI[sh_diap].Bmin) &&
                                    (mas_IRI[sh_IRI].Pel1 <= list_SP2_DistrIRI[sh_diap].Bmax)
                                   )
                                {
                                    // -------------------------------------------------------
                                    // Индекс нужного диапазона

                                    indSP = sh_diap;
                                    // -------------------------------------------------------
                                    // Ищем места по этой литере

                                    // FOR30
                                    for (shKan = 0; shKan < list_KanSP2_DistrIRI.Count; shKan++)
                                    {
                                        if (
                                            (list_KanSP2_DistrIRI[shKan].NumLit == list_SP2_DistrIRI[indSP].NumLit) &&
                                            (list_KanSP2_DistrIRI[shKan].flKan != 0)
                                          )
                                        {
                                            indKan = shKan;
                                            shKan = list_KanSP2_DistrIRI.Count; // For OOUT from FOR

                                            // Уменьшаем число мест по литере
                                            num_kan = list_KanSP2_DistrIRI[indKan].flKan;
                                            num_kan -= 1;
                                            objS_Kan_DistrIRI.flKan = num_kan;
                                            num_kan = list_KanSP2_DistrIRI[indKan].NumLit;
                                            objS_Kan_DistrIRI.NumLit = num_kan;
                                            list_KanSP2_DistrIRI.RemoveAt(indKan);
                                            list_KanSP2_DistrIRI.Insert(indKan, objS_Kan_DistrIRI);

                                            // Присваиваем этот ИРИ 2-й станции
                                            mas_IRI[sh_IRI].NSPRP = mas_SP[1].N_SP;
                                            mas_IRI[sh_IRI].NKanRP = list_KanSP2_DistrIRI[indKan].NumLit; // Litera
                                            mas_IRI[sh_IRI].PriznPop = 1;
                                            Count_IRI++;

                                        } // Litera is free

                                    } // FOR30 Kan
                                    // -------------------------------------------------------
                                    if (mas_IRI[sh_IRI].PriznPop == 1) // ИРИ распределился
                                        sh_diap = ndiap; // Выход из for

                                }

                            } // FOR 29 Diapazon


                        } // IF (PriznPop!=-1&&Prior2==4&&PriznPop!=1)


                    } // FOR 28 IRI
                    // ****************************************************************** SP2_2

                    // SP1(2)->SP2(1) *******************************************************
                    // Если остались ИРИ с приоритетом 2 для СП1, пытаемся перебросить их на 
                    // СП1 (если по ней у них приоритет 1)

                    // FOR31
                    for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
                    {
                        // IF (PriznPop!=-1&&Prior1==4&&PriznPop!=1)
                        if (
                            (mas_IRI[sh_IRI].PriznPop != -1) &&
                            (mas_IRI[sh_IRI].PriznPop != 1) &&  // ИРИ распределился
                            (mas_IRI[sh_IRI].Prior1 == 2)
                           )
                        {
                            // ---------------------------------------------------------------            
                            if (mas_IRI[sh_IRI].Prior2 == 0)
                                mas_IRI[sh_IRI].PriznPop = -1;
                            // ---------------------------------------------------------------            
                            else if (mas_IRI[sh_IRI].Prior2 == 1)
                            {

                                // Найти нужный диапазон в СП2
                                ndiap = list_SP2_DistrIRI.Count;
                                // FOR32
                                for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                                {
                                    if (
                                        (mas_IRI[sh_IRI].F_IRI >= list_SP2_DistrIRI[sh_diap].Fmin) &&
                                        (mas_IRI[sh_IRI].F_IRI <= list_SP2_DistrIRI[sh_diap].Fmax)
                                       //(mas_IRI[sh_IRI].Pel2 >= list_SP2_DistrIRI[sh_diap].Bmin) &&
                                       //(mas_IRI[sh_IRI].Pel2 <= list_SP2_DistrIRI[sh_diap].Bmax)
                                       )
                                    {
                                        // .........................................................
                                        // Индекс нужного диапазона

                                        indSP = sh_diap;
                                        // .........................................................
                                        // Ищем места по этой литере

                                        // FOR33
                                        for (shKan = 0; shKan < list_KanSP2_DistrIRI.Count; shKan++)
                                        {
                                            if (
                                                (list_KanSP2_DistrIRI[shKan].NumLit == list_SP2_DistrIRI[indSP].NumLit) &&
                                                (list_KanSP2_DistrIRI[shKan].flKan != 0)
                                              )
                                            {
                                                indKan = shKan;
                                                shKan = list_KanSP2_DistrIRI.Count; // For OOUT from FOR

                                                // Уменьшаем число мест по литере
                                                num_kan = list_KanSP2_DistrIRI[indKan].flKan;
                                                num_kan -= 1;
                                                objS_Kan_DistrIRI.flKan = num_kan;
                                                num_kan = list_KanSP2_DistrIRI[indKan].NumLit;
                                                objS_Kan_DistrIRI.NumLit = num_kan;
                                                list_KanSP2_DistrIRI.RemoveAt(indKan);
                                                list_KanSP2_DistrIRI.Insert(indKan, objS_Kan_DistrIRI);

                                                // Присваиваем этот ИРИ 2-й станции
                                                mas_IRI[sh_IRI].NSPRP = mas_SP[1].N_SP;
                                                mas_IRI[sh_IRI].NKanRP = list_KanSP2_DistrIRI[indKan].NumLit; // Litera
                                                mas_IRI[sh_IRI].PriznPop = 1;
                                                Count_IRI++;

                                            } // Litera is free

                                        } // FOR33 Kan
                                        // .........................................................
                                        if (mas_IRI[sh_IRI].PriznPop == 1) // ИРИ распределился
                                            sh_diap = ndiap; // Выход из for
                                        else
                                        {
                                            sh_diap = ndiap;
                                            mas_IRI[sh_IRI].PriznPop = -1;
                                        }

                                    } // IF(попал в диапазон)

                                } // FOR 32 Diapazon

                            } // IF(Prior2==3)
                            // ---------------------------------------------------------------            

                        } // IF (PriznPop!=-1&&Prior1==4&&PriznPop!=1)

                    } // FOR 31 IRI
                    // ******************************************************** SP1(2)->SP2(1)

                    // SP2(2)->SP1(1) *******************************************************
                    // Если остались ИРИ с приоритетом 2 для СП2, пытаемся перебросить их на 
                    // СП1 (если по ней у них приоритет 1)

                    // FOR34
                    for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
                    {
                        // IF (PriznPop!=-1&&Prior2==4&&PriznPop!=1)
                        if (
                            (mas_IRI[sh_IRI].PriznPop != -1) &&
                            (mas_IRI[sh_IRI].PriznPop != 1) &&  // ИРИ распределился
                            (mas_IRI[sh_IRI].Prior2 == 2)
                           )
                        {
                            // ---------------------------------------------------------------            
                            if (mas_IRI[sh_IRI].Prior1 == 0)
                                mas_IRI[sh_IRI].PriznPop = -1;
                            // ---------------------------------------------------------------            
                            else if (mas_IRI[sh_IRI].Prior1 == 1)
                            {

                                // Найти нужный диапазон в СП1
                                ndiap = list_SP1_DistrIRI.Count;
                                // FOR35
                                for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                                {
                                    if (
                                        (mas_IRI[sh_IRI].F_IRI >= list_SP1_DistrIRI[sh_diap].Fmin) &&
                                        (mas_IRI[sh_IRI].F_IRI <= list_SP1_DistrIRI[sh_diap].Fmax)
                                       //(mas_IRI[sh_IRI].Pel1 >= list_SP1_DistrIRI[sh_diap].Bmin) &&
                                       //(mas_IRI[sh_IRI].Pel1 <= list_SP1_DistrIRI[sh_diap].Bmax)
                                       )
                                    {
                                        // .........................................................
                                        // Индекс нужного диапазона

                                        indSP = sh_diap;
                                        // .........................................................
                                        // Ищем места по этой литере

                                        // FOR36
                                        for (shKan = 0; shKan < list_KanSP1_DistrIRI.Count; shKan++)
                                        {
                                            if (
                                                (list_KanSP1_DistrIRI[shKan].NumLit == list_SP1_DistrIRI[indSP].NumLit) &&
                                                (list_KanSP1_DistrIRI[shKan].flKan != 0)
                                              )
                                            {
                                                indKan = shKan;
                                                shKan = list_KanSP1_DistrIRI.Count; // For OOUT from FOR

                                                // Уменьшаем число мест по литере
                                                num_kan = list_KanSP1_DistrIRI[indKan].flKan;
                                                num_kan -= 1;
                                                objS_Kan_DistrIRI.flKan = num_kan;
                                                num_kan = list_KanSP1_DistrIRI[indKan].NumLit;
                                                objS_Kan_DistrIRI.NumLit = num_kan;
                                                list_KanSP1_DistrIRI.RemoveAt(indKan);
                                                list_KanSP1_DistrIRI.Insert(indKan, objS_Kan_DistrIRI);

                                                // Присваиваем этот ИРИ 1-й станции
                                                mas_IRI[sh_IRI].NSPRP = mas_SP[0].N_SP;
                                                mas_IRI[sh_IRI].NKanRP = list_KanSP1_DistrIRI[indKan].NumLit; // Litera
                                                mas_IRI[sh_IRI].PriznPop = 1;
                                                Count_IRI++;

                                            } // Litera is free

                                        } // FOR36 Kan
                                        // .........................................................
                                        if (mas_IRI[sh_IRI].PriznPop == 1) // ИРИ распределился
                                            sh_diap = ndiap; // Выход из for
                                        else
                                        {
                                            sh_diap = ndiap;
                                            mas_IRI[sh_IRI].PriznPop = -1;
                                        }

                                    } // IF(попал в диапазон)

                                } // FOR 35 Diapazon

                            } // IF(Prior1==3)
                            // ---------------------------------------------------------------            

                        } // IF (PriznPop!=-1&&Prior2==4&&PriznPop!=1)

                    } // FOR 34 IRI
                    // ******************************************************** SP2(2)->SP1(1)

                    // SP1_3 ******************************************************************
                    // Ostatki SP1,Prior3

                    // FOR33
                    for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
                    {
                        // IF (PriznPop!=-1&&Prior1==3)
                        if (
                            (mas_IRI[sh_IRI].PriznPop != -1) &&
                            (mas_IRI[sh_IRI].Prior1 == 3) &&
                            (mas_IRI[sh_IRI].PriznPop != 1)   // ИРИ распределился
                           )
                        {

                            // Найти нужный диапазон в СП1
                            ndiap = list_SP1_DistrIRI.Count;
                            // FOR14
                            for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                            {
                                if (
                                    (mas_IRI[sh_IRI].F_IRI >= list_SP1_DistrIRI[sh_diap].Fmin) &&
                                    (mas_IRI[sh_IRI].F_IRI <= list_SP1_DistrIRI[sh_diap].Fmax) &&
                                    (mas_IRI[sh_IRI].Pel1 >= list_SP1_DistrIRI[sh_diap].Bmin) &&
                                    (mas_IRI[sh_IRI].Pel1 <= list_SP1_DistrIRI[sh_diap].Bmax)
                                   )
                                {
                                    // -------------------------------------------------------
                                    // Индекс нужного диапазона

                                    indSP = sh_diap;
                                    // -------------------------------------------------------
                                    // Ищем места по этой литере

                                    // FOR15
                                    for (shKan = 0; shKan < list_KanSP1_DistrIRI.Count; shKan++)
                                    {
                                        if (
                                            (list_KanSP1_DistrIRI[shKan].NumLit == list_SP1_DistrIRI[indSP].NumLit) &&
                                            (list_KanSP1_DistrIRI[shKan].flKan != 0)
                                          )
                                        {
                                            indKan = shKan;
                                            shKan = list_KanSP1_DistrIRI.Count; // For OOUT from FOR

                                            // Уменьшаем число мест по литере
                                            num_kan = list_KanSP1_DistrIRI[indKan].flKan;
                                            num_kan -= 1;
                                            objS_Kan_DistrIRI.flKan = num_kan;
                                            num_kan = list_KanSP1_DistrIRI[indKan].NumLit;
                                            objS_Kan_DistrIRI.NumLit = num_kan;
                                            list_KanSP1_DistrIRI.RemoveAt(indKan);
                                            list_KanSP1_DistrIRI.Insert(indKan, objS_Kan_DistrIRI);

                                            // Присваиваем этот ИРИ 1-й станции
                                            mas_IRI[sh_IRI].NSPRP = mas_SP[0].N_SP;
                                            mas_IRI[sh_IRI].NKanRP = list_KanSP1_DistrIRI[indKan].NumLit; // Litera
                                            mas_IRI[sh_IRI].PriznPop = 1;
                                            Count_IRI++;

                                        } // Litera is free

                                    } // FOR15 Kan
                                    // -------------------------------------------------------
                                    if (mas_IRI[sh_IRI].PriznPop == 1) // ИРИ распределился
                                        sh_diap = ndiap; // Выход из for

                                }

                            } // FOR 14 Diapazon


                        } // IF (PriznPop!=-1&&Prior1==3)


                    } // FOR 33 IRI
                    // ****************************************************************** SP1_3

                    // SP2_3 ******************************************************************
                    // Ostatki SP2,Prior3

                    // FOR33
                    for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
                    {
                        // IF (PriznPop!=-1&&Prior1==3)
                        if (
                            (mas_IRI[sh_IRI].PriznPop != -1) &&
                            (mas_IRI[sh_IRI].Prior2 == 3) &&
                            (mas_IRI[sh_IRI].PriznPop != 1)   // ИРИ распределился
                           )
                        {

                            // Найти нужный диапазон в СП2
                            ndiap = list_SP2_DistrIRI.Count;
                            // FOR14
                            for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                            {
                                if (
                                    (mas_IRI[sh_IRI].F_IRI >= list_SP2_DistrIRI[sh_diap].Fmin) &&
                                    (mas_IRI[sh_IRI].F_IRI <= list_SP2_DistrIRI[sh_diap].Fmax) &&
                                    (mas_IRI[sh_IRI].Pel1 >= list_SP2_DistrIRI[sh_diap].Bmin) &&
                                    (mas_IRI[sh_IRI].Pel1 <= list_SP2_DistrIRI[sh_diap].Bmax)
                                   )
                                {
                                    // -------------------------------------------------------
                                    // Индекс нужного диапазона

                                    indSP = sh_diap;
                                    // -------------------------------------------------------
                                    // Ищем места по этой литере

                                    // FOR15
                                    for (shKan = 0; shKan < list_KanSP2_DistrIRI.Count; shKan++)
                                    {
                                        if (
                                            (list_KanSP2_DistrIRI[shKan].NumLit == list_SP2_DistrIRI[indSP].NumLit) &&
                                            (list_KanSP2_DistrIRI[shKan].flKan != 0)
                                          )
                                        {
                                            indKan = shKan;
                                            shKan = list_KanSP2_DistrIRI.Count; // For OOUT from FOR

                                            // Уменьшаем число мест по литере
                                            num_kan = list_KanSP2_DistrIRI[indKan].flKan;
                                            num_kan -= 1;
                                            objS_Kan_DistrIRI.flKan = num_kan;
                                            num_kan = list_KanSP2_DistrIRI[indKan].NumLit;
                                            objS_Kan_DistrIRI.NumLit = num_kan;
                                            list_KanSP2_DistrIRI.RemoveAt(indKan);
                                            list_KanSP2_DistrIRI.Insert(indKan, objS_Kan_DistrIRI);

                                            // Присваиваем этот ИРИ 2-й станции
                                            mas_IRI[sh_IRI].NSPRP = mas_SP[1].N_SP;
                                            mas_IRI[sh_IRI].NKanRP = list_KanSP2_DistrIRI[indKan].NumLit; // Litera
                                            mas_IRI[sh_IRI].PriznPop = 1;
                                            Count_IRI++;

                                        } // Litera is free

                                    } // FOR15 Kan
                                    // -------------------------------------------------------
                                    if (mas_IRI[sh_IRI].PriznPop == 1) // ИРИ распределился
                                        sh_diap = ndiap; // Выход из for

                                }

                            } // FOR 14 Diapazon


                        } // IF (PriznPop!=-1&&Prior1==3)


                    } // FOR 33 IRI
                    // ****************************************************************** SP2_3

                    // SP1_1 ******************************************************************
                    // Ostatki SP1,Prior1

                    // FOR33
                    for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
                    {
                        // IF (PriznPop!=-1&&Prior1==3)
                        if (
                            (mas_IRI[sh_IRI].PriznPop != -1) &&
                            (mas_IRI[sh_IRI].Prior1 == 1) &&
                            (mas_IRI[sh_IRI].PriznPop != 1)   // ИРИ распределился
                           )
                        {

                            // Найти нужный диапазон в СП1
                            ndiap = list_SP1_DistrIRI.Count;
                            // FOR14
                            for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                            {
                                if (
                                    (mas_IRI[sh_IRI].F_IRI >= list_SP1_DistrIRI[sh_diap].Fmin) &&
                                    (mas_IRI[sh_IRI].F_IRI <= list_SP1_DistrIRI[sh_diap].Fmax) &&
                                    (mas_IRI[sh_IRI].Pel1 >= list_SP1_DistrIRI[sh_diap].Bmin) &&
                                    (mas_IRI[sh_IRI].Pel1 <= list_SP1_DistrIRI[sh_diap].Bmax)
                                   )
                                {
                                    // -------------------------------------------------------
                                    // Индекс нужного диапазона

                                    indSP = sh_diap;
                                    // -------------------------------------------------------
                                    // Ищем места по этой литере

                                    // FOR15
                                    for (shKan = 0; shKan < list_KanSP1_DistrIRI.Count; shKan++)
                                    {
                                        if (
                                            (list_KanSP1_DistrIRI[shKan].NumLit == list_SP1_DistrIRI[indSP].NumLit) &&
                                            (list_KanSP1_DistrIRI[shKan].flKan != 0)
                                          )
                                        {
                                            indKan = shKan;
                                            shKan = list_KanSP1_DistrIRI.Count; // For OOUT from FOR

                                            // Уменьшаем число мест по литере
                                            num_kan = list_KanSP1_DistrIRI[indKan].flKan;
                                            num_kan -= 1;
                                            objS_Kan_DistrIRI.flKan = num_kan;
                                            num_kan = list_KanSP1_DistrIRI[indKan].NumLit;
                                            objS_Kan_DistrIRI.NumLit = num_kan;
                                            list_KanSP1_DistrIRI.RemoveAt(indKan);
                                            list_KanSP1_DistrIRI.Insert(indKan, objS_Kan_DistrIRI);

                                            // Присваиваем этот ИРИ 1-й станции
                                            mas_IRI[sh_IRI].NSPRP = mas_SP[0].N_SP;
                                            mas_IRI[sh_IRI].NKanRP = list_KanSP1_DistrIRI[indKan].NumLit; // Litera
                                            mas_IRI[sh_IRI].PriznPop = 1;
                                            Count_IRI++;

                                        } // Litera is free

                                    } // FOR15 Kan
                                    // -------------------------------------------------------
                                    if (mas_IRI[sh_IRI].PriznPop == 1) // ИРИ распределился
                                        sh_diap = ndiap; // Выход из for

                                }

                            } // FOR 14 Diapazon


                        } // IF (PriznPop!=-1&&Prior1==3)


                    } // FOR 33 IRI
                    // ****************************************************************** SP1_1

                    // SP2_1 ******************************************************************
                    // Ostatki SP2,Prior1

                    // FOR33
                    for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
                    {
                        // IF (PriznPop!=-1&&Prior1==3)
                        if (
                            (mas_IRI[sh_IRI].PriznPop != -1) &&
                            (mas_IRI[sh_IRI].Prior2 == 1) &&
                            (mas_IRI[sh_IRI].PriznPop != 1)   // ИРИ распределился
                           )
                        {

                            // Найти нужный диапазон в СП2
                            ndiap = list_SP2_DistrIRI.Count;
                            // FOR14
                            for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                            {
                                if (
                                    (mas_IRI[sh_IRI].F_IRI >= list_SP2_DistrIRI[sh_diap].Fmin) &&
                                    (mas_IRI[sh_IRI].F_IRI <= list_SP2_DistrIRI[sh_diap].Fmax) &&
                                    (mas_IRI[sh_IRI].Pel1 >= list_SP2_DistrIRI[sh_diap].Bmin) &&
                                    (mas_IRI[sh_IRI].Pel1 <= list_SP2_DistrIRI[sh_diap].Bmax)
                                   )
                                {
                                    // -------------------------------------------------------
                                    // Индекс нужного диапазона

                                    indSP = sh_diap;
                                    // -------------------------------------------------------
                                    // Ищем места по этой литере

                                    // FOR15
                                    for (shKan = 0; shKan < list_KanSP2_DistrIRI.Count; shKan++)
                                    {
                                        if (
                                            (list_KanSP2_DistrIRI[shKan].NumLit == list_SP2_DistrIRI[indSP].NumLit) &&
                                            (list_KanSP2_DistrIRI[shKan].flKan != 0)
                                          )
                                        {
                                            indKan = shKan;
                                            shKan = list_KanSP2_DistrIRI.Count; // For OOUT from FOR

                                            // Уменьшаем число мест по литере
                                            num_kan = list_KanSP2_DistrIRI[indKan].flKan;
                                            num_kan -= 1;
                                            objS_Kan_DistrIRI.flKan = num_kan;
                                            num_kan = list_KanSP2_DistrIRI[indKan].NumLit;
                                            objS_Kan_DistrIRI.NumLit = num_kan;
                                            list_KanSP2_DistrIRI.RemoveAt(indKan);
                                            list_KanSP2_DistrIRI.Insert(indKan, objS_Kan_DistrIRI);

                                            // Присваиваем этот ИРИ 2-й станции
                                            mas_IRI[sh_IRI].NSPRP = mas_SP[1].N_SP;
                                            mas_IRI[sh_IRI].NKanRP = list_KanSP2_DistrIRI[indKan].NumLit; // Litera
                                            mas_IRI[sh_IRI].PriznPop = 1;
                                            Count_IRI++;

                                        } // Litera is free

                                    } // FOR15 Kan
                                    // -------------------------------------------------------
                                    if (mas_IRI[sh_IRI].PriznPop == 1) // ИРИ распределился
                                        sh_diap = ndiap; // Выход из for

                                }

                            } // FOR 14 Diapazon


                        } // IF (PriznPop!=-1&&Prior1==3)


                    } // FOR 33 IRI
                    // ****************************************************************** SP2_1



                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! !!!RASPREDELENIE

                    if (Count_IRI == 0)
                        return -1;
                    else
                        return 0;
        */

        public static int f_DistributionIRI_SP(

                               // БД СП
                               List<ClassSP> mas_SP,

                               // БД ИРИ
                               List<ClassIRI> mas_IRI
                           )
        {
            // --------------------------------------------------------------------------------------------
            int nIRI = 0;
            int nSP = 0;
            int sh_IRI = 0;
            int sh_SP = 0;
            int sh_diap = 0;
            int ndiap = 0;
            int fl_prod = 0;
            int sh_lit = 0;
            double fmin1 = 0;
            double fmin1_dubl = 0;
            double fmax1 = 0;
            int fl_min = 0;
            int fl_min_dubl = 0;
            int num_kan = 0;
            int fl1_SP1 = 0;
            int fl2_SP2 = 0;

            // Расстояние между ИРИ и СП (dX=Xири-Xсп...)
            double dX = 0;
            double dY = 0;
            double dZ = 0;
            double D_IRI_SP1 = 0;
            double D_IRI_SP2 = 0;

            int prizn1 = 0;
            int prizn2 = 0;

            int indSP = 0;
            int indKan = 0;
            int shKan = 0;
            int Count_IRI = 0;
            // --------------------------------------------------------------------------------------------

            // Анализ входной информации ******************************************************************
            if ((mas_IRI.Count == 0) || (mas_SP.Count == 0))
                return -2;

            // Число ИРИ и СП
            nIRI = mas_IRI.Count;
            //nSP = mas_SP.Count;

            // ****************************************************************** Анализ входной информации

            // TEST_DIAP **********************************************************************************
            // Проверка:
            // вхождения ИРИ в переформированные диапазоны
            // вхождения ИРИ в запрещенные частоты

            // FOR5 IRI
            for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
            {
                nSP = mas_IRI[sh_IRI].lst_SP_IRI.Count;
                fmin1 = 0;
                // FOR6 SP
                for (sh_SP = 0; sh_SP < nSP; sh_SP++)
                {

                    // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    // Ищем в общем списке СП станцию с таким же номером 

                    int indx = -1;
                    indx = mas_SP.FindIndex(x => (x.N_SP == mas_IRI[sh_IRI].lst_SP_IRI[sh_SP].N_SP));

                    if (indx >= 0)
                    {
                        // .............................................................................
                        // Проверка вхождения в диапазоны углов и частот

                        ndiap = mas_SP[indx].mas_SP_IN_My.Count;

                        fl_min = 0;
                        // FOR7 диапазоны частот, углов
                        for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                        {
                            if (
                                (
                                 (mas_IRI[sh_IRI].F_IRI != -1) &&
                                 (mas_IRI[sh_IRI].F_IRI != 0) &&
                                 (mas_IRI[sh_IRI].F_IRI >= mas_SP[indx].mas_SP_IN_My[sh_diap].Fmin) &&
                                 (mas_IRI[sh_IRI].F_IRI <= mas_SP[indx].mas_SP_IN_My[sh_diap].Fmax) &&
                                 (mas_IRI[sh_IRI].lst_SP_IRI[sh_SP].Bearing == -1)

                                ) ||
                                (
                                 (mas_IRI[sh_IRI].F_IRI != -1) &&
                                 (mas_IRI[sh_IRI].F_IRI != 0) &&
                                 (mas_IRI[sh_IRI].F_IRI >= mas_SP[indx].mas_SP_IN_My[sh_diap].Fmin) &&
                                 (mas_IRI[sh_IRI].F_IRI <= mas_SP[indx].mas_SP_IN_My[sh_diap].Fmax) &&
                                 (mas_IRI[sh_IRI].lst_SP_IRI[sh_SP].Bearing != -1) &&
                                 (mas_IRI[sh_IRI].lst_SP_IRI[sh_SP].Bearing >= mas_SP[indx].mas_SP_IN_My[sh_diap].Beta_min) &&
                                 (mas_IRI[sh_IRI].lst_SP_IRI[sh_SP].Bearing <= mas_SP[indx].mas_SP_IN_My[sh_diap].Beta_max)
                                )
                               )
                            {
                                SP_IRI objSP_IRI7 = new SP_IRI();
                                objSP_IRI7 = mas_IRI[sh_IRI].lst_SP_IRI[sh_SP];
                                objSP_IRI7.PriznPop1 = 1;
                                mas_IRI[sh_IRI].lst_SP_IRI[sh_SP] = objSP_IRI7;

                                fl_min = 1;
                                sh_diap = ndiap + 1; // Выход из for7
                            }

                        } // FOR7 диапазоны частот, углов
                        // .............................................................................
                        // Проверка попадания в запрещенные частоты

                        ndiap = mas_SP[indx].mas_SP_IN_F.Count;

                        fl_min_dubl = 0;
                        // FOR8 диапазоны запрещенных частот
                        for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                        {
                            if (
                                 (mas_IRI[sh_IRI].F_IRI != -1) &&
                                 (mas_IRI[sh_IRI].F_IRI != 0) &&
                                 (mas_IRI[sh_IRI].F_IRI >= mas_SP[indx].mas_SP_IN_F[sh_diap].Fmin) &&
                                 (mas_IRI[sh_IRI].F_IRI <= mas_SP[indx].mas_SP_IN_F[sh_diap].Fmax)
                               )
                            {
                                fl_min_dubl = 1;
                                sh_diap = ndiap + 1; // Выход из for8
                            }

                        } // FOR8 диапазоны частот, углов
                        // .............................................................................

                    } // indx>=0

                    // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    // ИРИ попал в запрещенные частоты -> на эту СП не распределится
                    if (fl_min_dubl == 1)
                    {

                        SP_IRI objSP_IRI5 = new SP_IRI();
                        objSP_IRI5 = mas_IRI[sh_IRI].lst_SP_IRI[sh_SP];
                        objSP_IRI5.PriznPop = -1;
                        mas_IRI[sh_IRI].lst_SP_IRI[sh_SP] = objSP_IRI5;
                    }

                    // ИРИ НЕ попал в запрещенные частоты
                    else
                    {

                        // Не попал ни в один из диапазонов
                        if (fl_min == 0)
                        {
                            SP_IRI objSP_IRI6 = new SP_IRI();
                            objSP_IRI6 = mas_IRI[sh_IRI].lst_SP_IRI[sh_SP];
                            objSP_IRI6.PriznPop = -1;
                            mas_IRI[sh_IRI].lst_SP_IRI[sh_SP] = objSP_IRI6;
                        }

                    }
                    // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    if (mas_IRI[sh_IRI].lst_SP_IRI[sh_SP].PriznPop == 0)
                    {
                        // То есть есть возможность распределиться на эту станцию
                        fmin1 = 1;
                    }
                    // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                } // FOR6 -> SP

                // Нет возможности распределиться на станции, которые видят этот ИРИ
                if (fmin1 == 0)
                    mas_IRI[sh_IRI].NSPRP = -2;


            } // FOR5 IRI
              // ********************************************************************************** TEST_DIAP

            // Distrib_Coordinates ************************************************************************
            // Распределение ИРИ с координатами
            // !!! Теперь ИРИ сразу пытаемся распределять на станции, которые его видят:
            // - сначала на станции с пеленгом и ближайшие по расстоянию, потом на станции без пеленга и тоже по расстоянию
            // - в конце на станции без пеленга и без расстояния

            // ---------------------------------------------------------------------------------------------
            // Выстраиваем все массивы станций в порядке возрастания ds

            for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
            {
                mas_IRI[sh_IRI].lst_SP_IRI.Sort();

            } // FOR_IRI
            // ---------------------------------------------------------------------------------------------

            // ---------------------------------------------------------------------------------------------
            // FOR55 IRI
            for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
            {

                // IF*
                if (
                   (mas_IRI[sh_IRI].NSPRP != -2) &&
                   (mas_IRI[sh_IRI].F_IRI != -1) &&
                   (mas_IRI[sh_IRI].F_IRI != 0) &&
                   (mas_IRI[sh_IRI].fl_Raspr != 1) &&
                   (mas_IRI[sh_IRI].Latitude != -1) &&
                   (mas_IRI[sh_IRI].Longitude != -1)
                  )
                {
                    nSP = mas_IRI[sh_IRI].lst_SP_IRI.Count;

                    // СП с пеленгом &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
                    // Сначала перебираем СП с пеленгом

                    // FOR66 SP
                    for (sh_SP = 0; sh_SP < nSP; sh_SP++)
                    {
                        // IF**
                        if (
                           (mas_IRI[sh_IRI].lst_SP_IRI[sh_SP].PriznPop != -1) &&
                           (mas_IRI[sh_IRI].lst_SP_IRI[sh_SP].PriznPop1 == 1) &&
                           (mas_IRI[sh_IRI].lst_SP_IRI[sh_SP].Bearing != -1) &&
                           (mas_IRI[sh_IRI].lst_SP_IRI[sh_SP].ds != -1)
                          )
                        {

                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            // Ищем в общем списке СП станцию с таким же номером 

                            int indx1 = -1;
                            indx1 = mas_SP.FindIndex(x => (x.N_SP == mas_IRI[sh_IRI].lst_SP_IRI[sh_SP].N_SP));

                            if (indx1 >= 0)
                            {
                                // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                                // Проверка вхождения в диапазоны углов и частот (ищем индекс диапазона)

                                int indx2 = -1;
                                ndiap = mas_SP[indx1].mas_SP_IN_My.Count;
                                // FOR диапазоны частот, углов
                                for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                                {
                                    if (
                                        //(
                                        // (mas_IRI[sh_IRI].F_IRI >= mas_SP[indx1].mas_SP_IN_My[sh_diap].Fmin) &&
                                        // (mas_IRI[sh_IRI].F_IRI <= mas_SP[indx1].mas_SP_IN_My[sh_diap].Fmax) 

                                        //) ||
                                        (
                                         (mas_IRI[sh_IRI].F_IRI >= mas_SP[indx1].mas_SP_IN_My[sh_diap].Fmin) &&
                                         (mas_IRI[sh_IRI].F_IRI <= mas_SP[indx1].mas_SP_IN_My[sh_diap].Fmax) &&
                                         (mas_IRI[sh_IRI].lst_SP_IRI[sh_SP].Bearing >= mas_SP[indx1].mas_SP_IN_My[sh_diap].Beta_min) &&
                                         (mas_IRI[sh_IRI].lst_SP_IRI[sh_SP].Bearing <= mas_SP[indx1].mas_SP_IN_My[sh_diap].Beta_max)
                                        )
                                       )
                                    {
                                        // Индекс нужного диапазона                                        
                                        indx2 = sh_diap;
                                        sh_diap = ndiap + 1; // Выход из for7
                                    }

                                } // FOR диапазоны частот, углов
                                // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                                // Ищем место по литере

                                if (indx2 >= 0)
                                {
                                    // В массиве счетчиков ИРИ по литерам ищем индекс нужной литеры
                                    int indx3 = -1;
                                    indx3 = mas_SP[indx1].list_KanSPI_DistrIRI.FindIndex(x => (x.NumLit == mas_SP[indx1].mas_SP_IN_My[indx2].NumLit));
                                    if (indx3 >= 0)
                                    {
                                        // Есть ли свободное место
                                        if (mas_SP[indx1].list_KanSPI_DistrIRI[indx3].flKan != 0)
                                        {
                                            // Уменьшаем количество мест
                                            num_kan = mas_SP[indx1].list_KanSPI_DistrIRI[indx3].flKan;
                                            num_kan -= 1;
                                            S_Kan_DistrIRI obj10 = new S_Kan_DistrIRI();
                                            obj10 = mas_SP[indx1].list_KanSPI_DistrIRI[indx3];
                                            obj10.flKan = num_kan;
                                            mas_SP[indx1].list_KanSPI_DistrIRI[indx3] = obj10;
                                            // ИРИ распределился
                                            mas_IRI[sh_IRI].fl_Raspr = 1;
                                            mas_IRI[sh_IRI].NSPRP = mas_SP[indx1].N_SP;

                                            Count_IRI += 1;

                                            // Выход из FOR66
                                            sh_SP = nSP + 1;
                                        }


                                    } // if (indx3 >= 0)
                                } // if(indx2>=0)
                                // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                            } // indx1>=0
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                        } // IF**
                    } // FOR66 SP
                    // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& СП с пеленгом

                    // ИРИ без пеленга &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
                    // Если этот ИРИ не распределился на станции с пеленгом, перебираем станции без пеленга

                    if (mas_IRI[sh_IRI].fl_Raspr == 0)
                    {

                        // FOR66_1 SP
                        for (sh_SP = 0; sh_SP < nSP; sh_SP++)
                        {
                            // IF**
                            if (
                               (mas_IRI[sh_IRI].lst_SP_IRI[sh_SP].PriznPop != -1) &&
                               (mas_IRI[sh_IRI].lst_SP_IRI[sh_SP].PriznPop1 == 1) &&
                               (mas_IRI[sh_IRI].lst_SP_IRI[sh_SP].Bearing == -1)
                              )
                            {

                                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                // Ищем в общем списке СП станцию с таким же номером 

                                int indx1_1 = -1;
                                indx1_1 = mas_SP.FindIndex(x => (x.N_SP == mas_IRI[sh_IRI].lst_SP_IRI[sh_SP].N_SP));

                                if (indx1_1 >= 0)
                                {
                                    // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                                    // Проверка вхождения в диапазоны углов и частот (ищем индекс диапазона)

                                    int indx2_1 = -1;
                                    ndiap = mas_SP[indx1_1].mas_SP_IN_My.Count;
                                    // FOR диапазоны частот, углов
                                    for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                                    {
                                        if (
                                             (mas_IRI[sh_IRI].F_IRI >= mas_SP[indx1_1].mas_SP_IN_My[sh_diap].Fmin) &&
                                             (mas_IRI[sh_IRI].F_IRI <= mas_SP[indx1_1].mas_SP_IN_My[sh_diap].Fmax)
                                           )
                                        {
                                            // Индекс нужного диапазона                                        
                                            indx2_1 = sh_diap;
                                            sh_diap = ndiap + 1; // Выход из for диапазонов
                                        }

                                    } // FOR диапазоны частот, углов
                                      // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                                      // Ищем место по литере

                                    if (indx2_1 >= 0)
                                    {
                                        // В массиве счетчиков ИРИ по литерам ищем индекс нужной литеры
                                        int indx3_1 = -1;
                                        indx3_1 = mas_SP[indx1_1].list_KanSPI_DistrIRI.FindIndex(x => (x.NumLit == mas_SP[indx1_1].mas_SP_IN_My[indx2_1].NumLit));
                                        if (indx3_1 >= 0)
                                        {
                                            // Есть ли свободное место
                                            if (mas_SP[indx1_1].list_KanSPI_DistrIRI[indx3_1].flKan != 0)
                                            {
                                                // Уменьшаем количество мест
                                                num_kan = mas_SP[indx1_1].list_KanSPI_DistrIRI[indx3_1].flKan;
                                                num_kan -= 1;
                                                S_Kan_DistrIRI obj10_1 = new S_Kan_DistrIRI();
                                                obj10_1 = mas_SP[indx1_1].list_KanSPI_DistrIRI[indx3_1];
                                                obj10_1.flKan = num_kan;
                                                mas_SP[indx1_1].list_KanSPI_DistrIRI[indx3_1] = obj10_1;
                                                // ИРИ распределился
                                                mas_IRI[sh_IRI].fl_Raspr = 1;
                                                mas_IRI[sh_IRI].NSPRP = mas_SP[indx1_1].N_SP;

                                                Count_IRI += 1;

                                                // Выход из FOR66
                                                sh_SP = nSP + 1;
                                            }

                                        } // if (indx3 >= 0)
                                    } // if(indx2>=0)
                                      // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                                } // indx1>=0
                                  // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                            } // IF**
                        } // FOR66_1 SP

                    } // if(mas_IRI[sh_IRI].fl_Raspr==0)
                    // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& ИРИ без пеленга

                } // IF*


            } // FOR55 IRI

            // ************************************************************************ Distrib_Coordinates

            // Distrib_without_coordinates ****************************************************************
            // Распределение ИРИ без координат

            // ---------------------------------------------------------------------------------------------
            // FOR55_1 IRI
            for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
            {

                // IF*
                if (
                   (mas_IRI[sh_IRI].NSPRP != -2) &&
                   (mas_IRI[sh_IRI].F_IRI != -1) &&
                   (mas_IRI[sh_IRI].F_IRI != 0) &&
                   (mas_IRI[sh_IRI].fl_Raspr != 1) &&
                   ((mas_IRI[sh_IRI].Latitude == -1) || (mas_IRI[sh_IRI].Longitude == -1))
                  )
                {
                    nSP = mas_IRI[sh_IRI].lst_SP_IRI.Count;

                    // СП с пеленгом &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
                    // Сначала перебираем СП с пеленгом

                    // FOR66_2 SP
                    for (sh_SP = 0; sh_SP < nSP; sh_SP++)
                    {
                        // IF**
                        if (
                           (mas_IRI[sh_IRI].lst_SP_IRI[sh_SP].PriznPop != -1) &&
                           (mas_IRI[sh_IRI].lst_SP_IRI[sh_SP].PriznPop1 == 1) &&
                           (mas_IRI[sh_IRI].lst_SP_IRI[sh_SP].Bearing != -1)
                          //(mas_IRI[sh_IRI].lst_SP_IRI[sh_SP].ds != -1)
                          )
                        {

                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            // Ищем в общем списке СП станцию с таким же номером 

                            int indx1_2 = -1;
                            indx1_2 = mas_SP.FindIndex(x => (x.N_SP == mas_IRI[sh_IRI].lst_SP_IRI[sh_SP].N_SP));

                            if (indx1_2 >= 0)
                            {
                                // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                                // Проверка вхождения в диапазоны углов и частот (ищем индекс диапазона)

                                int indx2_2 = -1;
                                ndiap = mas_SP[indx1_2].mas_SP_IN_My.Count;
                                // FOR диапазоны частот, углов
                                for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                                {
                                    if (
                                         (mas_IRI[sh_IRI].F_IRI >= mas_SP[indx1_2].mas_SP_IN_My[sh_diap].Fmin) &&
                                         (mas_IRI[sh_IRI].F_IRI <= mas_SP[indx1_2].mas_SP_IN_My[sh_diap].Fmax) &&
                                         (mas_IRI[sh_IRI].lst_SP_IRI[sh_SP].Bearing >= mas_SP[indx1_2].mas_SP_IN_My[sh_diap].Beta_min) &&
                                         (mas_IRI[sh_IRI].lst_SP_IRI[sh_SP].Bearing <= mas_SP[indx1_2].mas_SP_IN_My[sh_diap].Beta_max)
                                       )
                                    {
                                        // Индекс нужного диапазона                                        
                                        indx2_2 = sh_diap;
                                        sh_diap = ndiap + 1; // Выход из for7
                                    }

                                } // FOR диапазоны частот, углов
                                // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                                // Ищем место по литере

                                if (indx2_2 >= 0)
                                {
                                    // В массиве счетчиков ИРИ по литерам ищем индекс нужной литеры
                                    int indx3_2 = -1;
                                    indx3_2 = mas_SP[indx1_2].list_KanSPI_DistrIRI.FindIndex(x => (x.NumLit == mas_SP[indx1_2].mas_SP_IN_My[indx2_2].NumLit));
                                    if (indx3_2 >= 0)
                                    {
                                        // Есть ли свободное место
                                        if (mas_SP[indx1_2].list_KanSPI_DistrIRI[indx3_2].flKan != 0)
                                        {
                                            // Уменьшаем количество мест
                                            num_kan = mas_SP[indx1_2].list_KanSPI_DistrIRI[indx3_2].flKan;
                                            num_kan -= 1;
                                            S_Kan_DistrIRI obj10_2 = new S_Kan_DistrIRI();
                                            obj10_2 = mas_SP[indx1_2].list_KanSPI_DistrIRI[indx3_2];
                                            obj10_2.flKan = num_kan;
                                            mas_SP[indx1_2].list_KanSPI_DistrIRI[indx3_2] = obj10_2;
                                            // ИРИ распределился
                                            mas_IRI[sh_IRI].fl_Raspr = 1;
                                            mas_IRI[sh_IRI].NSPRP = mas_SP[indx1_2].N_SP;

                                            Count_IRI += 1;

                                            // Выход из FOR66
                                            sh_SP = nSP + 1;
                                        }


                                    } // if (indx3_2 >= 0)
                                } // if(indx2_2>=0)
                                // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                            } // indx1_2>=0
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                        } // IF**
                    } // FOR66_2 SP
                    // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& СП с пеленгом

                    // ИРИ без пеленга &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
                    // Если этот ИРИ не распределился на станции с пеленгом, перебираем станции без пеленга

                    if (mas_IRI[sh_IRI].fl_Raspr == 0)
                    {

                        // FOR66_3 SP
                        for (sh_SP = 0; sh_SP < nSP; sh_SP++)
                        {
                            // IF**
                            if (
                               (mas_IRI[sh_IRI].lst_SP_IRI[sh_SP].PriznPop != -1) &&
                               (mas_IRI[sh_IRI].lst_SP_IRI[sh_SP].PriznPop1 == 1) &&
                               (mas_IRI[sh_IRI].lst_SP_IRI[sh_SP].Bearing == -1)
                              )
                            {

                                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                // Ищем в общем списке СП станцию с таким же номером 

                                int indx1_3 = -1;
                                indx1_3 = mas_SP.FindIndex(x => (x.N_SP == mas_IRI[sh_IRI].lst_SP_IRI[sh_SP].N_SP));

                                if (indx1_3 >= 0)
                                {
                                    // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                                    // Проверка вхождения в диапазоны углов и частот (ищем индекс диапазона)

                                    int indx2_3 = -1;
                                    ndiap = mas_SP[indx1_3].mas_SP_IN_My.Count;
                                    // FOR диапазоны частот, углов
                                    for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                                    {
                                        if (
                                             (mas_IRI[sh_IRI].F_IRI >= mas_SP[indx1_3].mas_SP_IN_My[sh_diap].Fmin) &&
                                             (mas_IRI[sh_IRI].F_IRI <= mas_SP[indx1_3].mas_SP_IN_My[sh_diap].Fmax)
                                           )
                                        {
                                            // Индекс нужного диапазона                                        
                                            indx2_3 = sh_diap;
                                            sh_diap = ndiap + 1; // Выход из for диапазонов
                                        }

                                    } // FOR диапазоны частот, углов
                                      // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                                      // Ищем место по литере

                                    if (indx2_3 >= 0)
                                    {
                                        // В массиве счетчиков ИРИ по литерам ищем индекс нужной литеры
                                        int indx3_3 = -1;
                                        indx3_3 = mas_SP[indx1_3].list_KanSPI_DistrIRI.FindIndex(x => (x.NumLit == mas_SP[indx1_3].mas_SP_IN_My[indx2_3].NumLit));
                                        if (indx3_3 >= 0)
                                        {
                                            // Есть ли свободное место
                                            if (mas_SP[indx1_3].list_KanSPI_DistrIRI[indx3_3].flKan != 0)
                                            {
                                                // Уменьшаем количество мест
                                                num_kan = mas_SP[indx1_3].list_KanSPI_DistrIRI[indx3_3].flKan;
                                                num_kan -= 1;
                                                S_Kan_DistrIRI obj10_3 = new S_Kan_DistrIRI();
                                                obj10_3 = mas_SP[indx1_3].list_KanSPI_DistrIRI[indx3_3];
                                                obj10_3.flKan = num_kan;
                                                mas_SP[indx1_3].list_KanSPI_DistrIRI[indx3_3] = obj10_3;
                                                // ИРИ распределился
                                                mas_IRI[sh_IRI].fl_Raspr = 1;
                                                mas_IRI[sh_IRI].NSPRP = mas_SP[indx1_3].N_SP;

                                                Count_IRI += 1;

                                                // Выход из FOR66
                                                sh_SP = nSP + 1;
                                            }

                                        } // if (indx3_3 >= 0)
                                    } // if(indx2_3>=0)
                                      // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                                } // indx1_3>=0
                                  // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                            } // IF**
                        } // FOR66_3 SP

                    } // if(mas_IRI[sh_IRI].fl_Raspr==0)
                    // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& ИРИ без пеленга

                } // IF*

            } // FOR55_1 IRI
            // **************************************************************** Distrib_without_coordinates


            // -------------------------------------------------------------------------------------------
            // Остались ИРИ, которые не удалось распределить (например, литера была занята)

            for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
            {
                // ИРИ не удалось распределиться
                if ((mas_IRI[sh_IRI].fl_Raspr == 0) && (mas_IRI[sh_IRI].NSPRP != -2))
                    mas_IRI[sh_IRI].NSPRP = -2;
            }
            // --------------------------------------------------------------------------------------------

            // 09.05 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            // Оставшиеся нераспределенные ИРИ пытаемся распределить на станции, которые не видят ИРИ
            // !!! Только по частоте

            // ---------------------------------------------------------------------------------------------
            // FOR55_1 IRI
            for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
            {

                // IF*
                if (
                   (mas_IRI[sh_IRI].NSPRP == -2) &&
                   (mas_IRI[sh_IRI].F_IRI != -1) &&
                   (mas_IRI[sh_IRI].F_IRI != 0) 
                  )
                {
                    nSP = mas_SP.Count;

                    // СП &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

                    // FOR66_2 SP
                    for (sh_SP = 0; sh_SP < nSP; sh_SP++)
                    {

                        // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        // Проверка попадания в запрещенные частоты

                        ndiap = mas_SP[sh_SP].mas_SP_IN_F.Count;

                        fl_min_dubl = 0;
                        // FOR8 диапазоны запрещенных частот
                        for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                        {
                            if (
                                 (mas_IRI[sh_IRI].F_IRI != -1) &&
                                 (mas_IRI[sh_IRI].F_IRI != 0) &&
                                 (mas_IRI[sh_IRI].F_IRI >= mas_SP[sh_SP].mas_SP_IN_F[sh_diap].Fmin) &&
                                 (mas_IRI[sh_IRI].F_IRI <= mas_SP[sh_SP].mas_SP_IN_F[sh_diap].Fmax)
                               )
                            {
                                fl_min_dubl = 1;
                                sh_diap = ndiap + 1; // Выход из for8
                            }

                        } // FOR8 диапазоны частот, углов
                          // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                        if (fl_min_dubl == 0)
                        {
                            // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                            // Проверка вхождения в диапазоны углов и частот (ищем индекс диапазона)

                            int indx2_2 = -1;
                            ndiap = mas_SP[sh_SP].mas_SP_IN_My.Count;
                            // FOR диапазоны частот, углов
                            for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                            {
                                if (
                                 (mas_IRI[sh_IRI].F_IRI != -1) &&
                                 (mas_IRI[sh_IRI].F_IRI != 0) &&
                                 (mas_IRI[sh_IRI].F_IRI >= mas_SP[sh_SP].mas_SP_IN_My[sh_diap].Fmin) &&
                                 (mas_IRI[sh_IRI].F_IRI <= mas_SP[sh_SP].mas_SP_IN_My[sh_diap].Fmax) 
                                   )
                                {
                                    // Индекс нужного диапазона                                        
                                    indx2_2 = sh_diap;
                                    sh_diap = ndiap + 1; // Выход из for7
                                }

                            } // FOR диапазоны частот, углов
                              // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                              // Ищем место по литере

                            if (indx2_2 >= 0)
                            {
                                // В массиве счетчиков ИРИ по литерам ищем индекс нужной литеры
                                int indx3_2 = -1;
                                indx3_2 = mas_SP[sh_SP].list_KanSPI_DistrIRI.FindIndex(x => (x.NumLit == mas_SP[sh_SP].mas_SP_IN_My[indx2_2].NumLit));
                                if (indx3_2 >= 0)
                                {
                                    // Есть ли свободное место
                                    if (mas_SP[sh_SP].list_KanSPI_DistrIRI[indx3_2].flKan != 0)
                                    {
                                        // Уменьшаем количество мест
                                        num_kan = mas_SP[sh_SP].list_KanSPI_DistrIRI[indx3_2].flKan;
                                        num_kan -= 1;
                                        S_Kan_DistrIRI obj10_2 = new S_Kan_DistrIRI();
                                        obj10_2 = mas_SP[sh_SP].list_KanSPI_DistrIRI[indx3_2];
                                        obj10_2.flKan = num_kan;
                                        mas_SP[sh_SP].list_KanSPI_DistrIRI[indx3_2] = obj10_2;
                                        // ИРИ распределился
                                        mas_IRI[sh_IRI].fl_Raspr = 1;
                                        mas_IRI[sh_IRI].NSPRP = mas_SP[sh_SP].N_SP;

                                        Count_IRI += 1;

                                        // Выход из FOR66
                                        sh_SP = nSP + 1;
                                    }


                                } // if (indx3_2 >= 0)
                            } // if(indx2_2>=0)
                              // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        }

                    } // FOR66_2 SP
                    // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& СП 


                } // IF*

            } // FOR55_1 IRI


            // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 09.05


            // --------------------------------------------------------------------------------------------
            if (Count_IRI == 0)
                return -1;
            else
                return 0;
            // --------------------------------------------------------------------------------------------

        } // P/P f_DistributionIRI_SP
          // ******************************************************************************** DISTR IRI

        // DISTR_IRI_MAIN ***************************************************************************
        // Целераспределение_Main (распределение ИРИ для N станций)


        /*
                //public static void Distribution(
                public static List<TableReconFWS> Distribution(



                                                 // Массив данных по СП
                                                 //ModelsTablesDBLib.TableASP[] tableAsp,
                                                 // Массив диапазонов углов и частот работы СП:
                                                 // [0] -> в нем содержится номер СП, один интервал
                                                 // [1] -> ........., другой интервал и т.д.
                                                 // !!! Для разных СП они м. идти вперемешку, т.е.
                                                 //     при формировании интервалов,например, для 1-й
                                                 //     СП н.перебрать весь массив, анализируя номер СП
                                                 //ModelsTablesDBLib.TableSectorsRangesSuppr[] rangesRP,
                                                 // Массив данных по ИРИ
                                                 //ModelsTablesDBLib.TableReconFWS[] DistribFWS,
                                                 // Кол-во ИРИ на одну литеру(для всех литер предполагается одинаково)
                                                 //int iCountIRI,
                                                 // Тип станции: 0- кол-во литер=7, !=0 - кол-во литер=9
                                                 // (пока у нас тип обеих станций имеется в виду одинаков)
                                                 //int TypeStation


                                                // 555
                                                // СП
                                                List<TableASP> tableAsp,
                                                // Сектора и диапазоны
                                                List<TableSectorsRanges> rangesRP,
                                                // Запрещенные частоты
                                                List<TableFreqSpec> freqForbidden,
                                                // ИРИ
                                                List<TableReconFWS> DistribFWS,
                                                // Кол-во ИРИ на одну литеру(для всех литер предполагается одинаково)
                                                int iCountIRI
                                               )
                {
                    // ---------------------------------------------------------------------------------------
                    int nSP_all = 0;
                    int nIRI_all = 0;
                    int flout = 0;
                    int shSP = 0;
                    int shIRI = 0;
                    int shLit = 0;
                    int shDiap = 0;
                    int shDiap1 = 0;
                    int nDiap = 0;
                    int nDiap1 = 0;
                    double b1 = 0;
                    double b2 = 0;
                    // ---------------------------------------------------------------------------------------
                    // 555

                    int MaxNumbLit = 10; // Number of liters
                    //if (TypeStation == 0)
                    //    MaxNumbLit = 7;
                    //else MaxNumbLit = 9;
                    // ---------------------------------------------------------------------------------------
                    // Кол-во SP, IRI

                    // 555
                    //nSP_all = tableAsp.Length;
                    //nIRI_all = DistribFWS.Length;
                    nSP_all = tableAsp.Count;
                    nIRI_all = DistribFWS.Count;

                    // ---------------------------------------------------------------------------------------
                    // Мои массивы для SP, IRI

                    S_SP_DistrIRI[] mas_SP = new S_SP_DistrIRI[nSP_all];
                    S_IRI_DistrIRI[] mas_IRI = new S_IRI_DistrIRI[nIRI_all];
                    // ---------------------------------------------------------------------------------------
                    // Переписываем массивы

                    // SP ************************************************************************************

                    for (shSP = 0; shSP < nSP_all; shSP++)
                    {
                        // Номер СП
                        mas_SP[shSP].N_SP = tableAsp[shSP].Id;

                        // Пересчет координат СП WGS84->прямоугольные координаты
                        f_DistributionIRI_Coord(
                                                tableAsp[shSP].Coordinates.Latitude,
                                                tableAsp[shSP].Coordinates.Longitude,
                                                ref mas_SP[shSP].X_SP,
                                                ref mas_SP[shSP].Y_SP
                                               );
                        mas_SP[shSP].Z_SP = 0;

                        // Кол-во ИРИ на одну литеру (имеется в виду одинаково для всех литер)
                        mas_SP[shSP].MaxIRILit = iCountIRI;

                        // Задание литер работы СП ...............................................................
                        // 555

                        // Резервируем массив для этой станции работы по литерам (1-работает по этой литере, 0-нет)
                        mas_SP[shSP].Lit = new int[MaxNumbLit];

                        for (shLit = 0; shLit < MaxNumbLit; shLit++)
                        {
                            //mas_SP[shSP].Lit[shLit] = 0;
                            mas_SP[shSP].Lit[shLit] = 1;
                        }

                                        //int MaxNumbLitTmp = 0;
                                        //MaxNumbLitTmp = tableAsp[shSP].Letters.Length;

                                        //if (MaxNumbLitTmp != 0)
                                        //{
                                        //    if (MaxNumbLitTmp >= MaxNumbLit)
                                        //    {
                                        //        for (shLit = 0; shLit < MaxNumbLit; shLit++)
                                        //        {
                                        //            mas_SP[shSP].Lit[shLit] = tableAsp[shSP].Letters[shLit];
                                        //        }
                                        //    }
                                        //    else
                                        //    {
                                        //        for (shLit = 0; shLit < MaxNumbLitTmp; shLit++)
                                        //        {
                                        //            mas_SP[shSP].Lit[shLit] = tableAsp[shSP].Letters[shLit];
                                        //        }
                                        //    }
                                        //} // MaxNumbLitTmp!=0
                        // ............................................................... Задание литер работы СП

                        // Диапазоны углов и частот СП ...........................................................

                        int numtmp = 0;

                        // 555
                        //nDiap = rangesRP.Length;
                        nDiap = rangesRP.Count;

                        shDiap1 = 0;

                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        // Подсчитываем число диапазонов углов и частот для данной станции

                        for (shDiap = 0; shDiap < nDiap; shDiap++)
                        {
                            numtmp = (int)rangesRP[shDiap].NumberASP;
                            // Это Нужная СП
                            // 555
                            if ((numtmp == mas_SP[shSP].N_SP) && (rangesRP[shDiap].IsCheck == true))
                            {
                                // 555
                                //b1 = (double)rangesRP[shDiap].AngleMin / 10d;
                                //b2 = (double)rangesRP[shDiap].AngleMax / 10d;
                                b1 = (double)rangesRP[shDiap].AngleMin;
                                b2 = (double)rangesRP[shDiap].AngleMax;

                                if (b1 <= b2)
                                {
                                    shDiap1 += 1;
                                }
                                else
                                {
                                    // Бьем диапазон на два при переходе через ноль градусов
                                    shDiap1 += 2;
                                }
                                nDiap1 = shDiap1;

                            } // Это Нужная СП

                        } // FOR по входному массиву
                          // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                          // Резервирую свой массив для диапазонов углов и частот

                        mas_SP[shSP].mas_SP_IN = new SP_IN[nDiap1];
                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        // Заполняю свой массив диапазонами углов и частот

                        shDiap1 = 0;

                        for (shDiap = 0; shDiap < nDiap; shDiap++)
                        {
                            numtmp = (int)rangesRP[shDiap].NumberASP;

                            // Это Нужная СП
                            // 555
                            if ((numtmp == mas_SP[shSP].N_SP) && (rangesRP[shDiap].IsCheck == true))
                            {
                                // 555
                                //b1 = (double)rangesRP[shDiap].AngleMin / 10d;
                                //b2 = (double)rangesRP[shDiap].AngleMax / 10d;
                                b1 = (double)rangesRP[shDiap].AngleMin;
                                b2 = (double)rangesRP[shDiap].AngleMax;

                                if (b1 <= b2)
                                {
                                    // 555
                                    //mas_SP[shSP].mas_SP_IN[shDiap1].Fmin = (double)rangesRP[shDiap].FreqMin / 10d;
                                    //mas_SP[shSP].mas_SP_IN[shDiap1].Fmin = (double)rangesRP[shDiap].FreqMax / 10d;
                                    mas_SP[shSP].mas_SP_IN[shDiap1].Fmin = (double)rangesRP[shDiap].FreqMinKHz;
                                    mas_SP[shSP].mas_SP_IN[shDiap1].Fmax = (double)rangesRP[shDiap].FreqMaxKHz;

                                    mas_SP[shSP].mas_SP_IN[shDiap1].Beta_min = b1;
                                    mas_SP[shSP].mas_SP_IN[shDiap1].Beta_max = b2;
                                    shDiap1 += 1;
                                }
                                else
                                {
                                    // 555
                                    //mas_SP[shSP].mas_SP_IN[shDiap1].Fmin = (double)rangesRP[shDiap].FreqMin / 10d;
                                    //mas_SP[shSP].mas_SP_IN[shDiap1].Fmin = (double)rangesRP[shDiap].FreqMax / 10d;
                                    mas_SP[shSP].mas_SP_IN[shDiap1].Fmin = (double)rangesRP[shDiap].FreqMinKHz;
                                    mas_SP[shSP].mas_SP_IN[shDiap1].Fmax = (double)rangesRP[shDiap].FreqMaxKHz;

                                    mas_SP[shSP].mas_SP_IN[shDiap1].Beta_min = b1;
                                    mas_SP[shSP].mas_SP_IN[shDiap1].Beta_max = 360;
                                    shDiap1 += 1;
                                    // 555
                                    //mas_SP[shSP].mas_SP_IN[shDiap1].Fmin = (double)rangesRP[shDiap].FreqMin / 10d;
                                    //mas_SP[shSP].mas_SP_IN[shDiap1].Fmin = (double)rangesRP[shDiap].FreqMax / 10d;
                                    mas_SP[shSP].mas_SP_IN[shDiap1].Fmin = (double)rangesRP[shDiap].FreqMinKHz;
                                    mas_SP[shSP].mas_SP_IN[shDiap1].Fmax = (double)rangesRP[shDiap].FreqMaxKHz;

                                    mas_SP[shSP].mas_SP_IN[shDiap1].Beta_min = 0;
                                    mas_SP[shSP].mas_SP_IN[shDiap1].Beta_max = b2;
                                    shDiap1 += 1;
                                }

                            } // Это Нужная СП

                        } // FOR по входному массиву
                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                        // ........................................................... Диапазоны углов и частот СП

                    } // FOR_SP
                    // ************************************************************************************ SP

                    // IRI ***********************************************************************************

                    for (shIRI = 0; shIRI < nIRI_all; shIRI++)
                    {
                        // ..................................................................................
                        // F
                        // 555

                        //mas_IRI[shIRI].F_IRI = (double)DistribFWS[shIRI].Freq / 10d;
                        mas_IRI[shIRI].F_IRI = (double)DistribFWS[shIRI].FreqKHz;
                        // ..................................................................................
                        // Пеленг от СП1
                        // 555

                        //if (DistribFWS[shIRI].Q != -1)
                        //    mas_IRI[shIRI].Pel1 = (double)DistribFWS[shIRI].Q / 10d;
                        //else
                        //    mas_IRI[shIRI].Pel1 = -1;

                        for (int shsp1 = 0; shsp1 < DistribFWS[shIRI].ListJamDirect.Count; shsp1++)
                        {
                            if (mas_SP[0].N_SP == DistribFWS[shIRI].ListJamDirect[shsp1].JamDirect.NumberASP)
                            {
                                mas_IRI[shIRI].Pel1 = (double)DistribFWS[shIRI].ListJamDirect[shsp1].JamDirect.Bearing;
                            }
                        }
                        // ..................................................................................
                        // Пеленг от СП2
                        // 555

                        //if (DistribFWS[shIRI].QN != -1)
                        //    mas_IRI[shIRI].Pel2 = (double)DistribFWS[shIRI].Q/ 10d;
                        //else
                        //    mas_IRI[shIRI].Pel2 = -1;

                        for (int shsp2 = 0; shsp2 < DistribFWS[shIRI].ListJamDirect.Count; shsp2++)
                        {
                            if (mas_SP[1].N_SP == DistribFWS[shIRI].ListJamDirect[shsp2].JamDirect.NumberASP)
                            {
                                mas_IRI[shIRI].Pel2 = (double)DistribFWS[shIRI].ListJamDirect[shsp2].JamDirect.Bearing;
                            }
                        }
                        // ..................................................................................
                        // Координаты ИРИ

                        if ((DistribFWS[shIRI].Coordinates.Latitude == -1) || (DistribFWS[shIRI].Coordinates.Longitude == -1))
                        {
                            mas_IRI[shIRI].X_IRI = 0;
                            mas_IRI[shIRI].Y_IRI = 0;
                        }
                        else
                        {
                            f_DistributionIRI_Coord(
                                                    DistribFWS[shIRI].Coordinates.Latitude,
                                                    DistribFWS[shIRI].Coordinates.Longitude,
                                                    ref mas_IRI[shIRI].X_IRI,
                                                    ref mas_IRI[shIRI].Y_IRI
                                                   );
                        }

                        mas_IRI[shIRI].Z_IRI = 0;
                        // ..................................................................................
                        // SP радиоразведки, от которой пришла информация

                        mas_IRI[shIRI].NSP1 = DistribFWS[shIRI].ListJamDirect[0].JamDirect.NumberASP;//.ASPRecon;
                        // ..................................................................................

                        mas_IRI[shIRI].PriznPop = 0;
                        mas_IRI[shIRI].Prior1 = 0;
                        mas_IRI[shIRI].Prior2 = 0;
                        // ..................................................................................
                        // Номер СП, на которую распределится ИРИ

                        // М.б. инициализировать -1? (если ни на какую не распределится, останется -1)
                        //?????????????
                        //mas_IRI[shIRI].NSPRP = 0;
                        mas_IRI[shIRI].NSPRP = -1;

                        mas_IRI[shIRI].NKanRP = 0;
                        // ..................................................................................
                        // Флаги попадания в запрещенные частоты (=1 -> попадает)
                        // 555

                        // SP1
                        mas_IRI[shIRI].fl_zapr1 = 0;
                        // SP2
                        mas_IRI[shIRI].fl_zapr2 = 0;
                        // ..................................................................................

                    } // FOR_IRI
                    // *********************************************************************************** IRI

                    // -----------------------------------------------------------------
                    // Функция распределения ИРИ по СП

                    flout = f_DistributionIRI_SP(

                                       // БД СП
                                       mas_SP,

                                       // 555
                                       // Тип станции
                                       //TypeStation,

                                       // 555
                                       // Запрещенные частоты
                                       freqForbidden,

                                       // БД ИРИ
                                       ref mas_IRI
                                       );

                    // -----------------------------------------------------------------
                    // Ни один ИРИ не распределился

                    //if(flout==-1)
                    //{
                    //    List<TableReconFWS> DistribFWS1 = new List<TableReconFWS>();
                    //    return DistribFWS1;
                    //}

                    // -----------------------------------------------------------------

                    // Изначально забиваем -2
                    for (shIRI = 0; shIRI < nIRI_all; shIRI++)
                    {

                        DistribFWS[shIRI].ASPSuppr = -2;

                    } // FOR_IRI

                    for (shIRI = 0; shIRI < nIRI_all; shIRI++)
                    {

                        DistribFWS[shIRI].ASPSuppr = mas_IRI[shIRI].NSPRP;

                    } // FOR_IRI
                    // -----------------------------------------------------------------

                    return DistribFWS;

                } // Целераспределение_Main

        */


        public static List<TableReconFWS> Distribution(

                                        // СП
                                        List<TableASP> tableAsp,
                                        // Сектора и диапазоны
                                        List<TableSectorsRanges> rangesRP,
                                        // Запрещенные частоты
                                        List<TableFreqSpec> freqForbidden,
                                        // ИРИ
                                        List<TableReconFWS> DistribFWS,
                                        // Кол-во ИРИ на одну литеру(для всех литер предполагается одинаково)
                                        int iCountIRI
                                       )

        {

            // ---------------------------------------------------------------------------------------
            int nSP_all = 0;
            int nIRI_all = 0;
            int flout = 0;
            int shSP = 0;
            int shIRI = 0;
            int shLit = 0;
            int shDiap = 0;
            int shDiap1 = 0;
            int nDiap = 0;
            int nDiap1 = 0;
            double b1 = 0;
            double b2 = 0;
            // ---------------------------------------------------------------------------------------
            int MaxNumbLit = 10; // Number of liters
            // ---------------------------------------------------------------------------------------
            // Кол-во SP, IRI

            nSP_all = tableAsp.Count;
            nIRI_all = DistribFWS.Count;
            // ---------------------------------------------------------------------------------------
            // Мои массивы для SP, IRI

            List<ClassSP> mas_SP = new List<ClassSP>();
            List<ClassIRI> mas_IRI = new List<ClassIRI>();
            // ---------------------------------------------------------------------------------------

            // SP ************************************************************************************

            // FOR*
            for (shSP = 0; shSP < nSP_all; shSP++)
            {
                // -----------------------------------------------------------------------------------
                ClassSP objClassSP = new ClassSP();
                objClassSP.Lit = new List<int>();
                objClassSP.mas_SP_IN = new List<SP_IN>();
                objClassSP.mas_SP_IN_F = new List<SP_IN>();
                objClassSP.mas_SP_IN_My = new List<SP_IN>();
                objClassSP.list_KanSPI_DistrIRI = new List<S_Kan_DistrIRI>();
                // -----------------------------------------------------------------------------------
                // Номер СП

                objClassSP.N_SP = tableAsp[shSP].Id;
                // -----------------------------------------------------------------------------------
                // Координаты

                objClassSP.Latitude = tableAsp[shSP].Coordinates.Latitude;
                objClassSP.Longitude = tableAsp[shSP].Coordinates.Longitude;
                // -----------------------------------------------------------------------------------
                // Количество ИРИ на литеру

                objClassSP.MaxIRILit = iCountIRI;
                // -----------------------------------------------------------------------------------
                // Задание литер работы СП
                // !!! Здесь считаем, что СП работают по всем литерам

                for (shLit = 0; shLit < MaxNumbLit; shLit++)
                {
                    int obji = 1;
                    objClassSP.Lit.Add(obji);
                }
                // -----------------------------------------------------------------------------------
                // Диапазоны углов и частот СП

                nDiap = rangesRP.Count;

                // FOR1
                for (shDiap = 0; shDiap < nDiap; shDiap++)
                {

                    if ((objClassSP.N_SP == rangesRP[shDiap].NumberASP) && (rangesRP[shDiap].IsCheck == true))
                    {
                        b1 = (double)rangesRP[shDiap].AngleMin;
                        b2 = (double)rangesRP[shDiap].AngleMax;

                        if (b1 <= b2)
                        {
                            SP_IN objSP_IN = new SP_IN();
                            objSP_IN.Fmin = (double)rangesRP[shDiap].FreqMinKHz;
                            objSP_IN.Fmax = (double)rangesRP[shDiap].FreqMaxKHz;
                            objSP_IN.Beta_min = b1;
                            objSP_IN.Beta_max = b2;
                            objClassSP.mas_SP_IN.Add(objSP_IN);
                        }
                        else
                        {
                            SP_IN objSP_IN1 = new SP_IN();
                            objSP_IN1.Fmin = (double)rangesRP[shDiap].FreqMinKHz;
                            objSP_IN1.Fmax = (double)rangesRP[shDiap].FreqMaxKHz;
                            objSP_IN1.Beta_min = b1;
                            objSP_IN1.Beta_max = 360;
                            objClassSP.mas_SP_IN.Add(objSP_IN1);

                            SP_IN objSP_IN2 = new SP_IN();
                            objSP_IN2.Fmin = (double)rangesRP[shDiap].FreqMinKHz;
                            objSP_IN2.Fmax = (double)rangesRP[shDiap].FreqMaxKHz;
                            objSP_IN2.Beta_min = 0;
                            objSP_IN2.Beta_max = b2;
                            objClassSP.mas_SP_IN.Add(objSP_IN2);
                        }

                    } // IF это нужная СП

                } // FOR1 по общему числу диапазонов
                // -----------------------------------------------------------------------------------
                // Добавляем элемент (СПi) в лист

                mas_SP.Add(objClassSP);
                // -----------------------------------------------------------------------------------
                // В добавленном элементе перераспределяем диапазоны

                if(mas_SP.Count>0)
                PereformRanges(mas_SP[mas_SP.Count - 1]);
                // -----------------------------------------------------------------------------------
                // В добавленном элементе формируем лист запрещенных частот

                nDiap = freqForbidden.Count;

                // FOR11
                for (shDiap = 0; shDiap < nDiap; shDiap++)
                {

                    //if ((mas_SP[mas_SP.Count - 1].N_SP == freqForbidden[shDiap].NumberASP) && (rangesRP[shDiap].IsCheck == true)&&
                    //    (mas_SP.Count>0))
                    if ((mas_SP[mas_SP.Count - 1].N_SP == freqForbidden[shDiap].NumberASP) &&
                    (mas_SP.Count > 0))
                        {

                            SP_IN objSP_IN3 = new SP_IN();
                        objSP_IN3.Fmin = (double)freqForbidden[shDiap].FreqMinKHz;
                        objSP_IN3.Fmax = (double)freqForbidden[shDiap].FreqMaxKHz;
                        mas_SP[mas_SP.Count - 1].mas_SP_IN_F.Add(objSP_IN3);

                    } // IF это нужная СП

                } // FOR11 по числу диапазонов
                // -----------------------------------------------------------------------------------

            } // FOR* -> SP
              // ************************************************************************************ SP

            // IRI **********************************************************************************

            double levelmin = -140;
            double levelmax = 0;

            // FOR**
            for (shIRI = 0; shIRI < nIRI_all; shIRI++)
            {

                // -----------------------------------------------------------------------------------
                ClassIRI objClassIRI = new ClassIRI();
                objClassIRI.lst_SP_IRI = new List<SP_IRI>();
                // -----------------------------------------------------------------------------------
                // ID

                objClassIRI.ID = DistribFWS[shIRI].Id;
                // -----------------------------------------------------------------------------------
                // Freq

                objClassIRI.F_IRI = (double)DistribFWS[shIRI].FreqKHz;
                // -----------------------------------------------------------------------------------
                // Coordinates

                objClassIRI.Latitude = DistribFWS[shIRI].Coordinates.Latitude;
                objClassIRI.Longitude = DistribFWS[shIRI].Coordinates.Longitude;
                // -----------------------------------------------------------------------------------
                objClassIRI.fl_Raspr = 0;
                objClassIRI.NSPRP = 0;
                // -----------------------------------------------------------------------------------

                // lst_SP_IRI >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                // Составляем список СП, которые видят этот ИРИ

                // FOR12
                for (shSP = 0; shSP < DistribFWS[shIRI].ListJamDirect.Count; shSP++)
                {
                    // ..............................................................................
                    SP_IRI objSP_IRI = new SP_IRI();
                    // ..............................................................................

                    // Эта СП видит этот ИРИ
                    //if ((DistribFWS[shIRI].ListJamDirect[shSP].JamDirect.Level >= levelmin) &&
                    //    (DistribFWS[shIRI].ListJamDirect[shSP].JamDirect.Level <= levelmax))
                   {
                        // '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        // Номер СП

                        objSP_IRI.N_SP = DistribFWS[shIRI].ListJamDirect[shSP].JamDirect.NumberASP;
                        // '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        // Ищем в общем списке СП станцию с таким же номером для взятия координат

                        int indx = -1;
                        double lt = -1;
                        double lng = -1;

                        indx = mas_SP.FindIndex(x => (x.N_SP == objSP_IRI.N_SP));
                        if (indx >= 0)
                        {
                            lt = mas_SP[indx].Latitude;
                            lng = mas_SP[indx].Longitude;
                        }
                        // '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        // Расстояние ИРИ_СП

                        if ((objClassIRI.Latitude != -1) && (objClassIRI.Longitude != -1) && (lt != -1) && (lng != -1))
                        {
                            objSP_IRI.ds = ClassBearing.f_D_2Points(objClassIRI.Latitude, objClassIRI.Longitude, lt, lng, 1);
                        }
                        // '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        // Пеленг от этой станции/-1

                        objSP_IRI.Bearing = DistribFWS[shIRI].ListJamDirect[shSP].JamDirect.Bearing;
                        // '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        objSP_IRI.PriznPop = 0;
                        objSP_IRI.PriznPop1 = 0;
                        objSP_IRI.Prior = 0;
                        // '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        objClassIRI.lst_SP_IRI.Add(objSP_IRI);
                        // '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    } // IF(Эта СП видит этот ИРИ)
                    // ..............................................................................

                } // FOR12 -> по списку СП в этом ИРИ

                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> lst_SP_IRI

                mas_IRI.Add(objClassIRI);

            } // FOR** -> IRI

            // ********************************************************************************** IRI

            // Raspred *****************************************************************************
            // Функция распределения ИРИ по СП

            flout = f_DistributionIRI_SP(

                               // БД СП
                               mas_SP,
                               // БД ИРИ
                               mas_IRI
                               );

            if (flout < 0)
            {
                DistribFWS.Clear();
            }
            else
            {
                for (shIRI = 0; shIRI < nIRI_all; shIRI++)
                {
                    DistribFWS[shIRI].ASPSuppr = mas_IRI[shIRI].NSPRP;
                } // FOR_IRI
            }
            // ***************************************************************************** Raspred

            return DistribFWS;

        }
        // *************************************************************************** DISTR_IRI_MAIN

        // PereformRanges ***************************************************************************
        // Переформирование массива диапазонов частот и секторов по углам для данной станции в связи 
        // с работой по литерам

        public static void PereformRanges(
                                       ClassSP objClassSP
                                       )
        {

            // ----------------------------------------------------------------
            List<S_F_DistrIRI> list_LIT_DistrIRI = new List<S_F_DistrIRI>();

            int MaxNumbLit = objClassSP.Lit.Count; // Number of liters
            // -----------------------------------------------------------------
            // Количество ИРИ и СП
            //int nIRI = 0;
            //int nSP = 0;
            //int sh_IRI = 0;
            //int sh_SP = 0;
            int sh_diap = 0;
            int ndiap = 0;
            int fl_prod = 0;
            int sh_lit = 0;
            double fmin1 = 0;
            double fmin1_dubl = 0;
            double fmax1 = 0;
            int fl_min = 0;
            int fl_min_dubl = 0;
            int num_kan = 0;
            //int fl1_SP1 = 0;
            //int fl2_SP2 = 0;
            int fl_SP = 0;

            // Расстояние между ИРИ и СП (dX=Xири-Xсп...)
            //double dX = 0;
            //double dY = 0;
            //double dZ = 0;
            //double D_IRI_SP1 = 0;
            //double D_IRI_SP2 = 0;

            //int prizn1 = 0;
            //int prizn2 = 0;

            //int indSP = 0;
            //int indKan = 0;
            //int shKan = 0;
            //int Count_IRI = 0;
            // -------------------------------------------------------------------------

            // Литеры *****************************************************************

            S_F_DistrIRI objS_F_DistrIRI = new S_F_DistrIRI();

            objS_F_DistrIRI.Fmin = 30000;
            objS_F_DistrIRI.Fmax = 50000;
            list_LIT_DistrIRI.Add(objS_F_DistrIRI);
            objS_F_DistrIRI.Fmin = 50000;
            objS_F_DistrIRI.Fmax = 90000;
            list_LIT_DistrIRI.Add(objS_F_DistrIRI);
            objS_F_DistrIRI.Fmin = 90000;
            objS_F_DistrIRI.Fmax = 160000;
            list_LIT_DistrIRI.Add(objS_F_DistrIRI);
            objS_F_DistrIRI.Fmin = 160000;
            objS_F_DistrIRI.Fmax = 290000;
            list_LIT_DistrIRI.Add(objS_F_DistrIRI);
            objS_F_DistrIRI.Fmin = 290000;
            objS_F_DistrIRI.Fmax = 512000;
            list_LIT_DistrIRI.Add(objS_F_DistrIRI);
            objS_F_DistrIRI.Fmin = 512000;
            objS_F_DistrIRI.Fmax = 860000;
            list_LIT_DistrIRI.Add(objS_F_DistrIRI);
            objS_F_DistrIRI.Fmin = 860000;
            objS_F_DistrIRI.Fmax = 1215000;
            list_LIT_DistrIRI.Add(objS_F_DistrIRI);
            objS_F_DistrIRI.Fmin = 1215000;
            objS_F_DistrIRI.Fmax = 2000000;
            list_LIT_DistrIRI.Add(objS_F_DistrIRI);
            objS_F_DistrIRI.Fmin = 2000000;
            objS_F_DistrIRI.Fmax = 3000000;
            list_LIT_DistrIRI.Add(objS_F_DistrIRI);
            objS_F_DistrIRI.Fmin = 3000000;
            objS_F_DistrIRI.Fmax = 6000000;
            list_LIT_DistrIRI.Add(objS_F_DistrIRI);

            // ***************************************************************** Литеры

            // mas_SP_IN-> mas_SP_IN_My ***********************************************
            // Преобразование входного массива для СП по частотам и углам в соответствии
            // с работой по литерам

            SP_IN objS_FB_DistrIRI = new SP_IN();
            S_Kan_DistrIRI objS_Kan_DistrIRI = new S_Kan_DistrIRI();


            ndiap = objClassSP.mas_SP_IN.Count;
            num_kan = 0;
            // ---------------------------------------------------------------------- 
            // FOR2 diap

            for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
            {

                fl_prod = 0;
                fl_min = 0;
                fl_min_dubl = 0;
                // ....................................................................
                // FOR3 litera

                for (sh_lit = 0; sh_lit < MaxNumbLit; sh_lit++)
                {

                    if (fl_prod == 1) goto vybmax;

                    if (fl_min_dubl == 1)
                    {

                        if (objClassSP.Lit[sh_lit] == 1)   // Litera active
                        {
                            fmin1 = fmin1_dubl;
                            fl_min = 1;
                            fl_min_dubl = 0;
                        }
                        else // Litera NO active
                        {
                            if (objClassSP.mas_SP_IN[sh_diap].Fmax > list_LIT_DistrIRI[sh_lit].Fmax)
                            {
                                fmin1_dubl = list_LIT_DistrIRI[sh_lit].Fmax;
                                fl_min = 0;
                                fl_min_dubl = 1;
                            }

                        }

                    } //fl_min_dubl==1


                    else
                    {
                        // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                        // fmin

                        // fmin этого диапазона меньше fmin Litera[0]
                        if (
                            (sh_lit == 0) &&
                            (objClassSP.mas_SP_IN[sh_diap].Fmin <= list_LIT_DistrIRI[sh_lit].Fmin)
                            )
                        {
                            if (objClassSP.Lit[sh_lit] == 1)   // Litera active
                            {
                                fmin1 = list_LIT_DistrIRI[sh_lit].Fmin;
                                fl_min = 1;
                                fl_min_dubl = 0;
                            }
                            else // Litera NO active
                            {
                                if (objClassSP.mas_SP_IN[sh_diap].Fmax > list_LIT_DistrIRI[sh_lit].Fmax)
                                {
                                    fmin1_dubl = list_LIT_DistrIRI[sh_lit].Fmax;
                                    fl_min = 0;
                                    fl_min_dubl = 1;
                                }

                            }

                        }
                        // ***
                        else // Находим литеру,где лежит fmin, если на этой станции эта литера активна
                        {
                            if (
                                (objClassSP.mas_SP_IN[sh_diap].Fmin >= list_LIT_DistrIRI[sh_lit].Fmin) &&
                                (objClassSP.mas_SP_IN[sh_diap].Fmin < list_LIT_DistrIRI[sh_lit].Fmax)
                               )
                            {
                                if (objClassSP.Lit[sh_lit] == 1) // Litera active
                                {
                                    fmin1 = objClassSP.mas_SP_IN[sh_diap].Fmin;
                                    fl_min = 1;
                                    fl_min_dubl = 0;

                                }
                                else  // Litera NO active
                                {
                                    if (objClassSP.mas_SP_IN[sh_diap].Fmax > list_LIT_DistrIRI[sh_lit].Fmax)
                                    {
                                        fmin1_dubl = list_LIT_DistrIRI[sh_lit].Fmax;
                                        fl_min = 0;
                                        fl_min_dubl = 1;
                                    }

                                }

                            }

                        } // else ***

                    } // fl_min_dubl==0
                      // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                      // fmax

                    vybmax: if (fl_min == 1)
                    {
                        // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        // Диапазон станции входит в диапазон литеры

                        if (
                            (objClassSP.mas_SP_IN[sh_diap].Fmax <= list_LIT_DistrIRI[sh_lit].Fmax)
                           )
                        {

                            // Литера неактивна-> переходим к следующему диапазону
                            if (objClassSP.Lit[sh_lit] == 0)
                            {
                                break;
                            }

                            fmax1 = objClassSP.mas_SP_IN[sh_diap].Fmax;
                            objS_FB_DistrIRI.Fmin = fmin1;
                            objS_FB_DistrIRI.Fmax = fmax1;
                            objS_FB_DistrIRI.Beta_min = objClassSP.mas_SP_IN[sh_diap].Beta_min;
                            objS_FB_DistrIRI.Beta_max = objClassSP.mas_SP_IN[sh_diap].Beta_max;
                            objS_FB_DistrIRI.NumLit = sh_lit;

                            objClassSP.mas_SP_IN_My.Add(objS_FB_DistrIRI);

                            //if (((sh_SP == 0) && (fl1_SP1 == 0)) ||
                            //    ((sh_SP == 1) && (fl2_SP2 == 0))) // 1-й раз
                            if (fl_SP == 0) // 1-й раз
                            {
                                objS_Kan_DistrIRI.flKan = objClassSP.MaxIRILit;
                                objS_Kan_DistrIRI.NumLit = sh_lit;
                                num_kan = sh_lit;
                                //if (sh_SP == 0)
                                //{
                                //    list_KanSP1_DistrIRI.Add(objS_Kan_DistrIRI);
                                //    fl1_SP1 = 1;
                                //}
                                //else
                                //{
                                //    list_KanSP2_DistrIRI.Add(objS_Kan_DistrIRI);
                                //    fl2_SP2 = 1;
                                //}

                                objClassSP.list_KanSPI_DistrIRI.Add(objS_Kan_DistrIRI);
                                fl_SP = 1;

                            }  // 1raz

                            else
                            {

                                objS_Kan_DistrIRI.flKan = objClassSP.MaxIRILit;
                                objS_Kan_DistrIRI.NumLit = sh_lit;
                                //if (sh_SP == 0)
                                //{
                                //    if (list_KanSP1_DistrIRI.FindIndex(x => x.NumLit == sh_lit) < 0)
                                //        list_KanSP1_DistrIRI.Add(objS_Kan_DistrIRI);
                                //}
                                //else
                                //{
                                //    if (list_KanSP2_DistrIRI.FindIndex(x => x.NumLit == sh_lit) < 0)
                                //        list_KanSP2_DistrIRI.Add(objS_Kan_DistrIRI);
                                //}


                                if (objClassSP.list_KanSPI_DistrIRI.FindIndex(x => x.NumLit == sh_lit) < 0)
                                    objClassSP.list_KanSPI_DistrIRI.Add(objS_Kan_DistrIRI);

                            } // NO 1 raz


                            // Переходим к следующему диапазону
                            break;

                        } // Диапазон станции входит в диапазон литеры
                          // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                          // fmax диапазона>fmax литеры

                        else
                        {
                            // 1111111111111111111111111111111111111111111111111111111111111111
                            if (fl_prod == 0)
                            {

                                fmax1 = list_LIT_DistrIRI[sh_lit].Fmax;
                                objS_FB_DistrIRI.Fmin = fmin1;
                                objS_FB_DistrIRI.Fmax = fmax1;
                                objS_FB_DistrIRI.Beta_min = objClassSP.mas_SP_IN[sh_diap].Beta_min;
                                objS_FB_DistrIRI.Beta_max = objClassSP.mas_SP_IN[sh_diap].Beta_max;
                                objS_FB_DistrIRI.NumLit = sh_lit;

                                //if (sh_SP == 0)
                                //    list_SP1_DistrIRI.Add(objS_FB_DistrIRI);
                                //else
                                //    list_SP2_DistrIRI.Add(objS_FB_DistrIRI);

                                objClassSP.mas_SP_IN_My.Add(objS_FB_DistrIRI);

                                //if (((sh_SP == 0) && (fl1_SP1 == 0)) ||
                                //    ((sh_SP == 1) && (fl2_SP2 == 0))) // 1-й раз
                                if (fl_SP == 0) // 1-й раз
                                {
                                    objS_Kan_DistrIRI.flKan = objClassSP.MaxIRILit;
                                    objS_Kan_DistrIRI.NumLit = sh_lit;
                                    num_kan = sh_lit;
                                    //if (sh_SP == 0)
                                    //{
                                    //    list_KanSP1_DistrIRI.Add(objS_Kan_DistrIRI);
                                    //    fl1_SP1 = 1;
                                    //}
                                    //else
                                    //{
                                    //    list_KanSP2_DistrIRI.Add(objS_Kan_DistrIRI);
                                    //    fl2_SP2 = 1;
                                    //}

                                    objClassSP.list_KanSPI_DistrIRI.Add(objS_Kan_DistrIRI);
                                    fl_SP = 1;

                                } // 1raz

                                else
                                {
                                    objS_Kan_DistrIRI.flKan = objClassSP.MaxIRILit;
                                    objS_Kan_DistrIRI.NumLit = sh_lit;
                                    //if (sh_SP == 0)
                                    //{
                                    //    if (list_KanSP1_DistrIRI.FindIndex(x => x.NumLit == sh_lit) < 0)
                                    //        list_KanSP1_DistrIRI.Add(objS_Kan_DistrIRI);
                                    //}
                                    //else
                                    //{
                                    //    if (list_KanSP2_DistrIRI.FindIndex(x => x.NumLit == sh_lit) < 0)
                                    //        list_KanSP2_DistrIRI.Add(objS_Kan_DistrIRI);
                                    //}

                                    if (objClassSP.list_KanSPI_DistrIRI.FindIndex(x => x.NumLit == sh_lit) < 0)
                                        objClassSP.list_KanSPI_DistrIRI.Add(objS_Kan_DistrIRI);

                                } // no 1 raz


                                fmin1 = list_LIT_DistrIRI[sh_lit].Fmax;
                                fl_min = 1;
                                fl_prod = 1;

                            } // if(fl_prod==0)
                              // 1111111111111111111111111111111111111111111111111111111111111111
                              // fl_prod==1

                            else
                            {
                                // Литера неактивна-> пропустить ее
                                if (objClassSP.Lit[sh_lit] == 0)
                                {
                                    fmin1 = list_LIT_DistrIRI[sh_lit].Fmax;
                                    fl_min = 1;
                                    fl_prod = 1;

                                }

                                // Литера активна -> включить полностью этот диапазон
                                else
                                {
                                    fmax1 = list_LIT_DistrIRI[sh_lit].Fmax;
                                    objS_FB_DistrIRI.Fmin = fmin1;
                                    objS_FB_DistrIRI.Fmax = fmax1;
                                    objS_FB_DistrIRI.Beta_min = objClassSP.mas_SP_IN[sh_diap].Beta_min;
                                    objS_FB_DistrIRI.Beta_max = objClassSP.mas_SP_IN[sh_diap].Beta_max;
                                    objS_FB_DistrIRI.NumLit = sh_lit;

                                    //if (sh_SP == 0)
                                    //    list_SP1_DistrIRI.Add(objS_FB_DistrIRI);
                                    //else
                                    //    list_SP2_DistrIRI.Add(objS_FB_DistrIRI);

                                    objClassSP.mas_SP_IN_My.Add(objS_FB_DistrIRI);

                                    //if (((sh_SP == 0) && (fl1_SP1 == 0)) ||
                                    //    ((sh_SP == 1) && (fl2_SP2 == 0))) // 1-й раз
                                    if (fl_SP == 0) // 1-й раз
                                    {
                                        objS_Kan_DistrIRI.flKan = objClassSP.MaxIRILit;
                                        objS_Kan_DistrIRI.NumLit = sh_lit;
                                        num_kan = sh_lit;
                                        //if (sh_SP == 0)
                                        //{
                                        //    list_KanSP1_DistrIRI.Add(objS_Kan_DistrIRI);
                                        //    fl1_SP1 = 1;
                                        //}
                                        //else
                                        //{
                                        //    list_KanSP2_DistrIRI.Add(objS_Kan_DistrIRI);
                                        //    fl2_SP2 = 1;
                                        //}

                                        objClassSP.list_KanSPI_DistrIRI.Add(objS_Kan_DistrIRI);
                                        fl_SP = 1;

                                    } // 1raz

                                    else
                                    {
                                        objS_Kan_DistrIRI.flKan = objClassSP.MaxIRILit;
                                        objS_Kan_DistrIRI.NumLit = sh_lit;
                                        //if (sh_SP == 0)
                                        //{
                                        //    if (list_KanSP1_DistrIRI.FindIndex(x => x.NumLit == sh_lit) < 0)
                                        //        list_KanSP1_DistrIRI.Add(objS_Kan_DistrIRI);
                                        //}
                                        //else
                                        //{
                                        //    if (list_KanSP2_DistrIRI.FindIndex(x => x.NumLit == sh_lit) < 0)
                                        //        list_KanSP2_DistrIRI.Add(objS_Kan_DistrIRI);
                                        //}

                                        if (objClassSP.list_KanSPI_DistrIRI.FindIndex(x => x.NumLit == sh_lit) < 0)
                                            objClassSP.list_KanSPI_DistrIRI.Add(objS_Kan_DistrIRI);

                                    } // no 1 raz

                                    fmin1 = list_LIT_DistrIRI[sh_lit].Fmax;
                                    fl_min = 1;
                                    fl_prod = 1;

                                } // SP works on this litera

                            } // fl_prod==1
                              // 1111111111111111111111111111111111111111111111111111111111111111

                        } // fmax диапазона>fmax литеры
                          // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    } // if(fl_min == 1)
                      // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

                } // FOR3 Litera
                  // ....................................................................

            } // FOR2 diap
              // ---------------------------------------------------------------------- 

            // *********************************************** mas_SP_IN-> mas_SP_IN_My


        }
        // *************************************************************************** PereformRanges


    } // Class
} // Namespace
