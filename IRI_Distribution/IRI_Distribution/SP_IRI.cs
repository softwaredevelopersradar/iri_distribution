﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRI_Distribution
{
    public class SP_IRI : IComparable
    {
        // Номер СП
        public int N_SP = 0;

        // Пеленг/-1
        public double Bearing = 0;

        // Расстояние ИРИ-СП (иниц. -1)
        public double ds = -1;

        // Признак возможности попадания ИРИ  (иниц. 0)
        // Не попадает в диапазон частот этой станции ->  =-1
        // Попадает в запрещенные частоты этой станции -> =-1
        // Имеет пеленг, но не попадает в рабочие диапазоны углов этой СП ->=-1
        // Эта СП не видит его ->=-1
        public int PriznPop;

        // (Иниц.0) =1 -> Попадает в диапазоны F и сектора B по этой станции
        public int PriznPop1 = 0;

        // Приоритет по этой станции (иниц.0)
        // 4 - имеет координаты и ds до этой станции =min
        // 3- имеет координаты и ds до этой станции > min-го
        // 2- без координат, есть пеленг
        // 1 - без координат, нет пеленга
        public int Prior = 0;


        public int CompareTo(object obj)
        {
            SP_IRI class1obj = (SP_IRI)obj;
            if (class1obj == null)
                throw new Exception("Null object");
            return this.ds.CompareTo(class1obj.ds);
        }

    } // Class
} // Namespace
