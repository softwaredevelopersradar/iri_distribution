﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRI_Distribution
{
    public class ClassSP
    {
        // Номер СП
        public int N_SP;

        public double Latitude;
        public double Longitude;

        // Макс. кол-во ИРИ на литеру
        public int MaxIRILit;

        // Массив работы по литерам (1-работает по этой литере)
        public List<int> Lit = new List<int>();

        // Диапазоны частот и сектора (углы)
        public List<SP_IN> mas_SP_IN = new List<SP_IN>();

        // Переформированный массив в соответствии с литерами
        public List<SP_IN> mas_SP_IN_My = new List<SP_IN>();

        // Запрещенные частоты (здесь используем только Fmin,Fmax)
        public List<SP_IN> mas_SP_IN_F = new List<SP_IN>();

        // Элемент листа содержит max кол-во ИРИ по литере и номер литеры рабочей
        public List<S_Kan_DistrIRI> list_KanSPI_DistrIRI = new List<S_Kan_DistrIRI>();

    } // Class
} // Namespace
