﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRI_Distribution
{
    public class ClassIRI
    {
        // ID IRI
        public int ID = 0;

        // FreqKHz
        public double F_IRI = 0;

        public double Latitude;
        public double Longitude;

        // =1 -> распределен на СП
        public int fl_Raspr = 0;

        // Номер СП, на которую распределился ИРИ
        // =-2 -> не распределился
        public int NSPRP = 0;

        // Относительные характеристики СП_ИРИ
        // !!! Только для СП, к-ые видят этот ИРИ
        public List<SP_IRI> lst_SP_IRI = new List<SP_IRI>();

    } // Class
} // Namespace
